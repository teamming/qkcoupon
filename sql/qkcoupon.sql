CREATE DATABASE IF NOT EXISTS `qkcoupon` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;

CREATE TABLE IF NOT EXISTS `qkcoupon`.`coupon_info` (
  `cid` BIGINT NOT NULL AUTO_INCREMENT COMMENT '基础优惠劵ID',
  `ctype` TINYINT(1) NOT NULL DEFAULT 0 COMMENT '优惠券类型',
  `utype` TINYINT(1) NOT NULL DEFAULT 0 COMMENT '用户类型',
  `lmode` TINYINT(1) NOT NULL DEFAULT 0 COMMENT '洗衣模式',
  `bid` VARCHAR(36) NOT NULL DEFAULT '' COMMENT '商家ID',
  `rwid` VARCHAR(100) NOT NULL DEFAULT '' COMMENT '洗衣房ID 与 洗衣机ID',
  `sum` INT NOT NULL DEFAULT 0 COMMENT '充值金额、红包金额等...',
  `discount` INT NOT NULL DEFAULT 0 COMMENT '赠送金额、折扣、满减等...',
  `max_discount` INT NOT NULL DEFAULT 0 COMMENT '限制的最大折扣值',
  `time_range` VARCHAR(200) NOT NULL DEFAULT '' COMMENT '限制日期时间范围',
  `start_tick` BIGINT NOT NULL DEFAULT 0 COMMENT '开始时间',
  `end_tick` BIGINT NOT NULL DEFAULT 0 COMMENT '结束时间',
  PRIMARY KEY (`cid`),
  INDEX `t_common_idx1` (`ctype` ASC),
  INDEX `t_common_idx2` (`bid` ASC))
ENGINE = MyISAM;

CREATE TABLE IF NOT EXISTS `qkcoupon`.`user_coupon` (
  `ccode` VARCHAR(36) NOT NULL COMMENT '用户优惠劵ID',
  `cid` BIGINT NOT NULL DEFAULT 0 COMMENT '基础优惠劵ID',
  `uid` VARCHAR(36) NOT NULL DEFAULT '' COMMENT '用户ID',
  `cstate` TINYINT(1) NOT NULL DEFAULT 1 COMMENT '优惠券状态',
  `create_tick` BIGINT NOT NULL DEFAULT 0 COMMENT '创建时间',
  `use_tick` BIGINT NOT NULL DEFAULT 0 COMMENT '使用时间',
  PRIMARY KEY (`ccode`),
  INDEX `t_user_idx2` (`uid` ASC),
  INDEX `t_user_idx3` (`cstate` ASC),
  INDEX `t_user_idx4` (`create_tick` ASC))
ENGINE = MyISAM;

CREATE TABLE IF NOT EXISTS `qkcoupon`.`share_info` (
  `share_id` VARCHAR(36) NOT NULL COMMENT '分享ID',
  `sharer` VARCHAR(36) NOT NULL DEFAULT '' COMMENT '分享者',
  `add_num` INT NOT NULL DEFAULT 0 COMMENT '已领的优惠劵数量',
  `total_num` INT NOT NULL DEFAULT 0 COMMENT '总共的优惠劵数量',
  `expire_tick` BIGINT NOT NULL DEFAULT 0 COMMENT '分享的过期时间',
  PRIMARY KEY (`share_id`))
ENGINE = MyISAM;

CREATE TABLE IF NOT EXISTS `qkcoupon`.`receive_share` (
  `ccode` VARCHAR(36) NOT NULL COMMENT '用户优惠劵ID',
  `share_id` VARCHAR(36) NOT NULL DEFAULT '' COMMENT '分享ID',
  `cid` BIGINT NOT NULL DEFAULT 0 COMMENT '基础优惠劵ID',
  PRIMARY KEY (`ccode`),
  INDEX `receive_share_idx1` (`share_id` ASC))
ENGINE = MyISAM;

CREATE TABLE IF NOT EXISTS `qkcoupon`.`share_cid` (
  `share_id` VARCHAR(36) NOT NULL,
  `cid` BIGINT NOT NULL,
  UNIQUE INDEX `t_share_cid_idx1` (`share_id` ASC, `cid` ASC),
  INDEX `t_share_cid_idx2` (`share_id` ASC))
ENGINE = MyISAM;

CREATE TABLE IF NOT EXISTS `qkcoupon`.`setting` (
  `k` VARCHAR(36) NOT NULL COMMENT 'share_rule: 分享规则',
  `v` VARCHAR(1000) NOT NULL,
  PRIMARY KEY (`k`))
ENGINE = MyISAM;

INSERT INTO `qkcoupon`.`setting` (`k`,`v`)VALUES('share_rule','{\"rules\":[{\"otype\":1,\"rule\": [{\"ctype\":2,\"discount\":960,\"count\":8,\"expiry_day\":4},{\"ctype\":2,\"discount\":920,\"count\":2,\"expiry_day\":3}]},{\"otype\":2,\"rule\": [{\"ctype\":1,\"sum\":2000,\"discount\":100,\"count\":5,\"expiry_day\":15},{\"ctype\":1,\"sum\":3000,\"discount\":200,\"count\":2,\"expiry_day\":15},{\"ctype\":1,\"sum\":5000,\"discount\":400,\"count\":1,\"expiry_day\":15},{\"ctype\":1,	\"sum\":6000,\"discount\":600,\"count\":1,\"expiry_day\":15},{	\"ctype\":1,	\"sum\":10000,\"discount\":1000,\"count\":1,\"expiry_day\":15}]}]}');
INSERT INTO `qkcoupon`.`setting` (`k`,`v`)VALUES('recommend_rule','{ "rules":[ { "operator":1, "rule":[{"ctype":3,"count":1,"expiry_day":7} ] }, { "operator":2, "rule": [{"ctype":3,"count":1,"expiry_day":7} ] } ]}');

ALTER TABLE `qkcoupon`.`coupon_info` ADD COLUMN total_num INT NOT NULL DEFAULT 0 AFTER time_range;
ALTER TABLE `qkcoupon`.`coupon_info` ADD COLUMN add_num INT NOT NULL DEFAULT 0 AFTER time_range;
ALTER TABLE `qkcoupon`.`coupon_info` DROP COLUMN is_collar;

ALTER TABLE `qkcoupon`.`user_coupon` ADD COLUMN mobile VARCHAR(15) NOT NULL DEFAULT '' AFTER uid;
ALTER TABLE `qkcoupon`.`user_coupon` ADD INDEX t_user_idx5 (`mobile`);
ALTER TABLE `qkcoupon`.`user_coupon` ALTER COLUMN uid SET DEFAULT '';

CREATE TABLE IF NOT EXISTS `qkcoupon`.`receive_recommend` (
  `ccode` VARCHAR(36) NOT NULL COMMENT '用户优惠劵ID',
  `recommend_id` VARCHAR(36) NULL COMMENT '推荐ID',
  `cid` BIGINT NULL COMMENT '基础优惠劵ID',
  `receiver` VARCHAR(36) NULL COMMENT '接收者',
  `return` TINYINT(1) NULL DEFAULT 0 COMMENT '是否返还',
  PRIMARY KEY (`ccode`),
  INDEX `receiver_recommend_idx1` (`recommend_id` ASC),
  UNIQUE INDEX `receiver_recommend_idx2` (`receiver` ASC))
ENGINE = MyISAM;

CREATE TABLE IF NOT EXISTS `qkcoupon`.`recommend_info` (
  `recommend_id` VARCHAR(36) NOT NULL COMMENT '推荐ID',
  PRIMARY KEY (`recommend_id`))
ENGINE = MyISAM;

CREATE TABLE IF NOT EXISTS `qkcoupon`.`recommend_cid` (
  `recommend_id` VARCHAR(36) NOT NULL COMMENT '推荐ID',
  `cid` BIGINT NOT NULL COMMENT '基础优惠劵ID',
  `operator` TINYINT NULL COMMENT '0:推荐者\n1:接收者',
  UNIQUE INDEX `recommend_cid_idx1` (`recommend_id` ASC, `cid` ASC),
  INDEX `recommend_cid_idx2` (`recommend_id` ASC))
ENGINE = MyISAM;

CREATE TABLE IF NOT EXISTS `user_coupon_1705` (
  `ccode` varchar(36) NOT NULL DEFAULT '' COMMENT '优惠卷编号',
  `mobile` varchar(36) NOT NULL DEFAULT '' COMMENT '领取者手机号',
  `insert_tick` bigint(20) NOT NULL DEFAULT '0' COMMENT '插入时间',
  `is_old` tinyint(4) NOT NULL DEFAULT '0' COMMENT '是否是老用户,0:新用户,1:老用户',
  `origin` int(11) NOT NULL DEFAULT '1' COMMENT '来源:1,用户填写;2,后台发放,0,老用户',
  INDEX `user_coupon_1705_idx1` (`ccode`),
  INDEX `user_coupon_1705_idx2` (`mobile`),
  INDEX `user_coupon_1705_idx3` (`insert_tick`)
)

CREATE TABLE IF NOT EXISTS `qkcoupon`.`exchange_info` (
  `exchange_id` VARCHAR(36) NOT NULL DEFAULT '' COMMENT '优惠码信息ID',
  `exchange_text` VARCHAR(50) NOT NULL DEFAULT '' COMMENT '优惠码文本',
  `available` TINYINT NOT NULL DEFAULT 1 COMMENT '优惠码可用性',
  `bid` VARCHAR(36) NOT NULL DEFAULT '' COMMENT '商家ID',
  `new_user` TINYINT NOT NULL DEFAULT 0 COMMENT '是否仅限新用户',
  `user_limit` INT NOT NULL DEFAULT 1 COMMENT '一个用户可以兑换的次数',
  `coupon_count` INT NOT NULL DEFAULT 0 COMMENT '已兑换的次数',
  `coupon_limit` INT NOT NULL DEFAULT 0 COMMENT '优惠码可以兑换的次数上限',
  `start_tick` BIGINT NOT NULL DEFAULT 0 COMMENT '有效开始时间',
  `end_tick` BIGINT NOT NULL DEFAULT 0 COMMENT '有效结束时间',
  `insert_tick` BIGINT NOT NULL DEFAULT 0 COMMENT '插入时间',
  `update_tick` BIGINT NOT NULL DEFAULT 0 COMMENT '更改时间',
  PRIMARY KEY (`exchange_id`),
  INDEX `exchange_info_idx1` (`bid` ASC),
  INDEX `exchange_info_idx2` (`start_tick` ASC),
  INDEX `exchange_info_idx3` (`end_tick` ASC),
  INDEX `exchange_info_idx4` (`exchange_text` ASC))
ENGINE = MyISAM;

CREATE TABLE IF NOT EXISTS `qkcoupon`.`exchange_rule` (
  `exchange_id` VARCHAR(36) NOT NULL COMMENT '优惠码信息ID',
  `rule` VARCHAR(2000) NOT NULL COMMENT '优惠码对应的优惠卷生成规则',
  PRIMARY KEY (`exchange_id`))
ENGINE = MyISAM;

CREATE TABLE IF NOT EXISTS `qkcoupon`.`exchange_count` (
  `exchange_id` VARCHAR(36) NOT NULL DEFAULT '' COMMENT '兑换码',
  `receiver` VARCHAR(36) NOT NULL DEFAULT '' COMMENT '兑换者ID',
  `exchange_count` INT NOT NULL DEFAULT 0 COMMENT '积累兑换次数',
  PRIMARY KEY (`exchange_id`,`receiver`),
  INDEX `exchange_count_idx1` (`receiver` ASC))
ENGINE = MyISAM;

CREATE TABLE IF NOT EXISTS `qkcoupon`.`receive_exchange` (
  `ccode` VARCHAR(36) NOT NULL DEFAULT '' COMMENT '用户优惠劵ID',
  `exchange_id` VARCHAR(36) NOT NULL DEFAULT '' COMMENT '优惠码信息ID',
  `receiver` VARCHAR(36) NOT NULL DEFAULT '' COMMENT '兑换者ID',
  `cid` BIGINT NULL DEFAULT 0 COMMENT '基础优惠劵ID',
  PRIMARY KEY (`ccode`),
  INDEX `receive_exchange_idx1` (`exchange_id` ASC))
ENGINE = MyISAM;

ALTER TABLE `qkcoupon`.`coupon_info` MODIFY COLUMN `rwid` VARCHAR(1000);



