/*
 * Copyrignt (c) xuzeshui.com. All Rights Reserved.
 * Author: Zeshui Xu<i@xuzeshui.com>
 * Created Time: 2015-08-22 14:38:49
 * Last Modified: 2016-11-22 21:03:03
 * File Name: auth/appkey.go
 * Description:
 */
package auth

//"sync"

var (
	authAppKey *authAppKeyService
)

//每个appkey的基本信息
type AppKeyItem struct {
	Desc      string //appkey的描述
	SecretKey string //私钥
}

//AppKey说明
const (
	QK_APP_SELF    string = "qkapp self"
	QK_CC          string = "qkcc"
	QINGKE_ANDROID string = "qingke android"
	QINGKE_IOS     string = "qingke ios"
	QINGKE_H5      string = "qingke h5"
	QINGKE_WECHAT  string = "qingke wechat"
)

//AppKey
const (
	APPKEY_QK_APP_SELF    string = "82047331ece848f4889019164d53fdc0"
	APPKEY_QK_CC          string = "3d02c3a03f484b72baaba7aab9e17938"
	APPKEY_QINGKE_ANDROID string = "271d8700b1074a9582a8cc4cdff94c5f"
	APPKEY_QINGKE_IOS     string = "0272050d21d641e2b6f188fde6e48476"
	APPKEY_QINGKE_H5      string = "62ac59ca9ba2415898438c36dcc34f53"
	APPKEY_QINGKE_WECHAT  string = "e3b01549970b4a65a3b29f8e2c12939b"
)

//AppKey Salt
const (
	SALT_QK_APP_SELF    string = "ecb938ba93f34378b9b701fee3f71b96"
	SALT_QK_CC          string = "c220a9f643fc41de925028b99c2a9523"
	SALT_QINGKE_ANDROID string = "9c9babfb8a3f4bb48d4ea651368585f2"
	SALT_QINGKE_IOS     string = "4a25a6e2719648789640f9254dd862a9"
	SALT_QINGKE_H5      string = "585d035ae9584f0f84ce6aa1500ed2ab"
	SALT_QINGKE_WECHAT  string = "ca1ffef7a98a42f6b2042fe8ce82fc73"
)

func newAppKeyItem(desc, secrect string) *AppKeyItem {
	return &AppKeyItem{
		Desc:      desc,
		SecretKey: secrect,
	}
}

type authAppKeyService struct {
	appKeyInfo map[string]*AppKeyItem
	//appkeyInfoLock *sync.RWMutex
}

/*
	构造authAppKeyService实例
*/
func newAuthAppKeyService() *authAppKeyService {
	service := &authAppKeyService{
		appKeyInfo: make(map[string]*AppKeyItem),
		//appkeyInfoLock: new(sync.RWMutex),
	}
	service.initAppKeyInfo()
	return service
}

func GetAuthAppKeyService() *authAppKeyService {
	if authAppKey == nil {
		authAppKey = newAuthAppKeyService()
	}
	return authAppKey
}

//初始化加载配置,当前为硬编码
func (p *authAppKeyService) initAppKeyInfo() {
	p.appKeyInfo[APPKEY_QK_APP_SELF] = newAppKeyItem(QK_APP_SELF, SALT_QK_APP_SELF)
	p.appKeyInfo[APPKEY_QK_CC] = newAppKeyItem(QK_CC, SALT_QK_CC)
	p.appKeyInfo[APPKEY_QINGKE_ANDROID] = newAppKeyItem(QINGKE_ANDROID, SALT_QINGKE_ANDROID)
	p.appKeyInfo[APPKEY_QINGKE_IOS] = newAppKeyItem(QINGKE_IOS, SALT_QINGKE_IOS)
	p.appKeyInfo[APPKEY_QINGKE_H5] = newAppKeyItem(QINGKE_H5, SALT_QINGKE_H5)
}

//验证appkey是否有效
func (p *authAppKeyService) ValidateAppKey(appkey string) bool {
	_, ok := p.GetAppKeyItem(appkey)
	return ok
}

//根据appkey获取appkey的基本信息
func (p *authAppKeyService) GetAppKeyItem(appkey string) (*AppKeyItem, bool) {
	item, ok := p.appKeyInfo[appkey]
	return item, ok
}

//判断是否为轻客后台服务
func (p *authAppKeyService) IsQkAppSelft(appkey string) bool {
	item, ok := p.appKeyInfo[appkey]
	if !ok {
		//如果找不到appkey，表示是非法的
		return false
	}

	if item.Desc != QK_APP_SELF {
		return false
	}

	return true
}

//获取appkey对应的私钥
func (p *authAppKeyService) GetSecretKey(appkey string) (string, bool) {
	item, ok := p.GetAppKeyItem(appkey)
	if !ok {
		return "", false
	}
	return item.SecretKey, true
}
