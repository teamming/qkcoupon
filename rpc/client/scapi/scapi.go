/*
 * Copyrignt (c) qkier.com. All Rights Reserved.
 * Author: zengxiaobao@qkier.com
 * Created Time: 2017-07-29 15:37:54
 * Last Modified: 2017-07-29 15:38:22
 * File Name: scapi.go
 * Description:
 */

//
package scapi

// package main

import (
	"fmt"

	"doubimeizhi.com/utility"

	"github.com/astaxie/beego"
	"github.com/astaxie/beego/httplib"
)

var (
	schost   string = "apisc.qkier.com"
	scAppkey string = "d5eb1d9af9304cd3a4b622ef54098726"
	scSecret string = "553a863f5b9c4e95acd53490210cbd82"
)

type SCSessionCheckResp struct {
	Code   int    `json:"code"`
	Msg    string `json:"msg"`
	Userid string `json:"userid"`
	Name   string `json:"name"`
}

func SCSessionCheck(sessionid string) *SCSessionCheckResp {
	nusb := utility.NewURLSignBuilder(scAppkey, scSecret, fmt.Sprintf("https://%s/v1/account/sessioncheck?", schost))
	nusb.SetURLParam("sessionid", sessionid)
	reqURL := nusb.BuildURL()
	b := httplib.Get(reqURL)
	resp := &SCSessionCheckResp{}
	if err := b.ToJSON(resp); err != nil {
		return nil
	}
	return resp
}

func init() {
	schost = beego.AppConfig.DefaultString("backsvr::scapi_host", "apisc.qkier.com")
	scAppkey = beego.AppConfig.DefaultString("backsvr::scapi_appkey", "d5eb1d9af9304cd3a4b622ef54098726")
	scSecret = beego.AppConfig.DefaultString("backsvr::scapi_secret", "553a863f5b9c4e95acd53490210cbd82")
}

// func main() {
// 	info := SCSessionCheck("8de06eaab0c842ff9641cf7ef36937b7")
// 	fmt.Println(info)
// }
