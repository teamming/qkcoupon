package qingke

import (
	"crypto/tls"
	"fmt"
	"net/url"
	"qkcoupon/models"
	"time"

	"qkcoupon/auth"
	"qkcoupon/errorcode"

	"doubimeizhi.com/utility"
	"github.com/astaxie/beego"
	"github.com/astaxie/beego/httplib"
	log "github.com/cihub/seelog"
)

const (
	DEFAULT_QK_HOST string = "qk.app.qkier.com"
	APPKEY          string = "140a6d51d67c477e83cd91a241678824"
)

var (
	QkHost string = DEFAULT_QK_HOST
)

/*
 *@note 根据mode_id查询洗衣机信息
 *@param mode_id: 洗衣模式编号
 *@return *QSQkCouponWashInfo: 洗衣机信息
 */
func GetWashingmodeQueryByModeId(token string, mode_id int64) (*models.QSQkCouponWashInfo, uint) {
	v := url.Values{}

	v.Set("appkey", "271d8700b1074a9582a8cc4cdff94c5f")
	v.Set("ak", token)
	v.Set("mode_id", utility.ToString(mode_id))

	now := time.Now().Unix()
	v.Set("time", utility.ToString(now))

	sign := auth.GetURLSignService().CalcURLSign(v)

	reqURL := fmt.Sprintf("https://%s/washer/washingmode/query/bymodeid?appkey=271d8700b1074a9582a8cc4cdff94c5f&ak=%s&time=%d&mode_id=%d&sign=%s", QkHost, token, now, mode_id, sign)

	log.Debug(reqURL)

	b := httplib.Get(reqURL)
	b.SetHost(DEFAULT_QK_HOST)
	b.Debug(true)
	b.SetTLSClientConfig(&tls.Config{InsecureSkipVerify: true})

	rsp := &models.QSQkCouponWashInfoRsp{}
	if err := b.ToJSON(rsp); err != nil {
		log.Warn(err.Error())
		return nil, errorcode.EC_GL_INVALID_JSON_BODY
	}

	log.Debugf("QSQkCouponWashInfoRsp:%v", rsp)

	code := uint(rsp.Code)
	if code != errorcode.EC_GL_SUCCESS {
		return nil, code
	}

	return rsp.Data, errorcode.EC_GL_SUCCESS
}

type CheckNewUserRsp struct {
	Code uint   `json:"code"`
	Msg  string `json:"msg"`
}

/*
 *@note 根据mode_id查询洗衣机信息
 *@param mode_id: 洗衣模式编号
 *@return *QSQkCouponWashInfo: 洗衣机信息
 */
func CheckNewUser(token string, uid string) (bool, uint) {
	v := url.Values{}

	v.Set("appkey", "271d8700b1074a9582a8cc4cdff94c5f")
	v.Set("ak", token)

	now := time.Now().Unix()
	v.Set("time", utility.ToString(now))

	sign := auth.GetURLSignService().CalcURLSign(v)

	reqURL := fmt.Sprintf("https://%s/nu/new/user?appkey=271d8700b1074a9582a8cc4cdff94c5f&ak=%s&time=%d&uid=%s&sign=%s", QkHost, token, now, uid, sign)

	log.Debug(reqURL)

	b := httplib.Get(reqURL)
	b.SetHost(DEFAULT_QK_HOST)
	b.Debug(true)
	b.SetTLSClientConfig(&tls.Config{InsecureSkipVerify: true})

	rsp := &CheckNewUserRsp{}
	if err := b.ToJSON(rsp); err != nil {
		log.Warn(err.Error())
		return false, errorcode.EC_GL_INTERNAL_ERROR
	}

	log.Debugf("CheckNewUserRsp:%v", rsp)

	if rsp.Code != 0 {
		return false, errorcode.EC_GL_SUCCESS
	}

	return true, errorcode.EC_GL_SUCCESS
}

// WashRoomInfoRsp 查询洗衣房信息的返回值
type WashRoomInfoRsp struct {
	Code uint                             `json:"code"`
	Msg  string                           `json:"msg"`
	Data []*models.QSQkCouponWashRoomInfo `json:"data"`
}

/*
 *@note 根据rid查询洗衣房名称
 *@param rids: 洗衣房id列表
 *@return []*models.QSQkCouponWashRoomInfo: 洗衣房信息
 */
func GetRoomInfo(rids []string, token string, uid string) ([]*models.QSQkCouponWashRoomInfo, uint) {
	v := url.Values{}

	v.Set("appkey", "271d8700b1074a9582a8cc4cdff94c5f")
	v.Set("ak", token)

	now := time.Now().Unix()
	v.Set("time", utility.ToString(now))

	sign := auth.GetURLSignService().CalcURLSign(v)

	reqURL := fmt.Sprintf("https://%s/league/washroom/query/byroomids/batch?appkey=271d8700b1074a9582a8cc4cdff94c5f&ak=%s&time=%d&uid=%s&sign=%s", QkHost, token, now, uid, sign)

	log.Debug(reqURL)

	b := httplib.Get(reqURL)
	b.SetHost(DEFAULT_QK_HOST)
	b.Debug(true)
	b.SetTLSClientConfig(&tls.Config{InsecureSkipVerify: true})

	rsp := &WashRoomInfoRsp{}
	if err := b.ToJSON(rsp); err != nil {
		log.Warn(err.Error())
		return nil, errorcode.EC_GL_INTERNAL_ERROR
	}

	log.Debugf("CheckNewUserRsp:%v", rsp)

	if rsp.Code != 0 {
		return nil, errorcode.EC_GL_SUCCESS
	}

	return rsp.Data, errorcode.EC_GL_SUCCESS
}

func init() {
	QkHost = beego.AppConfig.DefaultString("host::QkHost", DEFAULT_QK_HOST)
}
