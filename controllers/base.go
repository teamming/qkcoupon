/*
 * Copyrignt (c) xuzeshui.com. All Rights Reserved.
 * Author: Zeshui Xu<i@xuzeshui.com>
 * Created Time: 2016-01-19 21:01:32
 * Last Modified: 2017-08-07 12:00:31
 * File Name: controllers/base.go
 * Description:接入层的基类定义,主要进行权限判断
 */
package controllers

import (
	"strings"
	"time"

	"qkcoupon/rpc/client/passport"
	"qkcoupon/rpc/client/scapi"

	"qkcoupon/auth"

	"qkcoupon/errorcode"

	"github.com/astaxie/beego"
	log "github.com/cihub/seelog"
)

/*
	最基础验证,只做Appkey的存在性检验
*/
type BaseController struct {
	beego.Controller
	StartTime   time.Time
	Resp        map[string]interface{} //输出结果
	AppKey      string                 //Appkey值
	ReqTime     int64                  //请求时间
	AccessToken string                 //当前登录的用户的Token
	Scheme      string                 //协议的类型http,https
	Sign        string                 //URL签名
}

/*
	在Method方法前调用该函数
*/
func (p *BaseController) Prepare() {
	p.Resp = make(map[string]interface{})
	p.StartTime = time.Now()
	p.AppKey = p.GetString("appkey", "")
	p.Scheme = p.Ctx.Input.Header("X-Scheme")

	//验证Appkey是否合法
	if !auth.GetAuthAppKeyService().ValidateAppKey(p.AppKey) {
		p.Resp["code"] = errorcode.EC_GL_INVALID_APPKEY
		p.Output("BaseController::Base")
		p.StopRun()
		return
	}

	p.ForceHTTPS()
}

func (p *BaseController) ForceHTTPS() {
	if p.Scheme != "https" {
		p.Resp["code"] = errorcode.EC_GL_INVALID_HTTPS
		p.Output("Prepare(Https)")
		p.StopRun()
	}
}

func (p *BaseController) Output(funcName string) {
	v, ok := p.Resp["code"]
	if !ok {
		log.Warnf("fun:%s, code not exist!", funcName)
		return
	}

	p.Resp["msg"] = errorcode.Msg(v.(uint))

	timeend := time.Since(p.StartTime)
	log.Infof("%s, ip: %s, %s %s, cost: %s, Resp:(%d, %s)", funcName,
		p.Ctx.Input.IP(), p.Ctx.Input.Method(), p.Ctx.Input.URI(),
		timeend.String(), p.Resp["code"], p.Resp["msg"])
	p.Ctx.Output.Header("Access-Control-Allow-Origin", "*")

	//p.Resp["msg"] = errorcode.MsgShow(v.(uint))

	p.Data["json"] = p.Resp
	p.ServeJSON()
}

/*
	判断是否管理员权限,可以适当放宽权限
*/
func (p *BaseController) PriorityVisit() bool {
	return p.isInternalIP(p.Ctx.Input.IP())
}

func (p *BaseController) isInternalIP(clientIP string) bool {
	return strings.HasPrefix(clientIP, "172.") ||
		clientIP == "127.0.0.1" ||
		clientIP == "0.0.0.0"
}

/*
	URL签名验证
*/
type URLSignController struct {
	BaseController
}

func (p *URLSignController) Prepare() {
	p.BaseController.Prepare()

	if p.PriorityVisit() {
		return
	}

	if ec, _, ok := auth.GetURLSignService().ValidateURLParam(p.Ctx.Request.URL.Query()); !ok {
		p.Resp["code"] = ec
		p.Output("Prepare(URLSign)")
		p.StopRun()
		return
	}
}

/*
	用户登录验证
*/
type LoginedController struct {
	URLSignController
	AccessToken string
	Userid      string
}

func (p *LoginedController) Prepare() {
	p.URLSignController.Prepare()

	p.AccessToken = p.GetString("ak", "")
	p.Userid = p.GetString("uid", "")

	//log.Debug(p)

	if beego.AppConfig.String("RunMode") != "dev" {
		//开发环境不验证AccessToken
		if !passport.IsValidAccessTokenForUserid(p.AccessToken, p.Userid) {
			p.Resp["code"] = errorcode.EC_GL_INVALID_ACCESS_TOKEN
			p.Output("Prepare(Logined)")
			p.StopRun()
			return
		}
	}
}

/*
	可选用户登录
*/
type LoginedOptionalController struct {
	URLSignController
	AccessToken string
	Userid      string
	IsLogined   bool
}

func (p *LoginedOptionalController) Prepare() {
	p.URLSignController.Prepare()

	p.IsLogined = false
	p.AccessToken = p.GetString("ak", "")
	p.Userid = p.GetString("uid", "")

	if passport.IsValidAccessTokenForUserid(p.AccessToken, p.Userid) {
		p.IsLogined = true //登录OK
	}
}

/*
	管理员登录
*/
type BackendAdminController struct {
	BaseController
}

func (p *BackendAdminController) Prepare() {
	p.BaseController.Prepare()

	if !p.PriorityVisit() {
		p.Resp["code"] = errorcode.EC_GL_INVALID_IP
		p.Output("Prepare(BackendAdmin)")
		p.StopRun()
		return
	}
}

/*
   后台管理登录
*/
type SCLoginedController struct {
	BaseController
	SessionId string
	Userid    string
	Name      string
}

func (p *SCLoginedController) Prepare() {
	p.SessionId = p.GetString("sessionid", "")
	p.BaseController.Prepare()

	if p.PriorityVisit() {
		return
	}

	if ec, _, ok := auth.GetURLSignService().ValidateURLParam(p.Ctx.Request.URL.Query()); !ok {
		p.Resp["code"] = ec
		p.Output("Prepare(Logined)")
		p.StopRun()
		return
	}

	if p.SessionId == "" {
		p.Resp["code"] = errorcode.EC_GL_INVALID_SESSIONID
		p.Output("Prepare(SCLogined Empty Sessionid)")
		p.StopRun()
	}

	if resp := scapi.SCSessionCheck(p.SessionId); resp == nil {
		p.Resp["code"] = errorcode.EC_GL_INVALID_SESSIONID
		p.Output("Prepare(SCLogined remote resp nil)")
		p.StopRun()
	} else {
		if resp.Code != 0 {
			p.Resp["code"] = errorcode.EC_GL_INVALID_SESSIONID
			p.Output("Prepare(SCLogined invalid sessionid)")
			p.StopRun()
		} else {
			p.Userid = resp.Userid
			p.Name = resp.Name
		}
	}
}
