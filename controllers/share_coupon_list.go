/*
 * Copyrignt (c) doubimeizhi.com. All Rights Reserved.
 * Author: mr.gao<fbg@live.com>
 * Created Time: 2016-12-07 15:06
 * Last Modified: 2017-07-29 15:44:53
 * File Name: qkcoupon/controllers/share_coupon_list.go
 * Description:
 */

package controllers

import (
	"fmt"
	"qkcoupon/db"
	"qkcoupon/errorcode"
	"qkcoupon/rpc/client/passport"
	"qkcoupon/utils"

	"qkcoupon/models"
)

type ShareCouponListController struct {
	URLSignController
}

/*
 *@note  验证请求参数
 *@return (uint, string, string): (错误码, uid, share_id)
 */
func (p *ShareCouponListController) verification() (uint, string, string) {
	var errCode uint = errorcode.EC_GL_SUCCESS
	var uid string = ""
	var share_id string = ""

	for {
		share_id = p.GetString("share_id", "")
		/*if share_id == "" {
			log.Warn("share_id == ''")
			errCode = errorcode.EC_GL_INVALID_URL_PARAMS
			break
		}*/

		break
	}

	return errCode, uid, share_id
}

/*
 *@note  服务端数据转客户端数据
 *@param sscl: *models.PSQkCouponShareCouponList
 *@return *models.PCQkCouponShareCouponList
 */
func (p *ShareCouponListController) sShareCouponList2CShareCouponList(sscl *models.PSQkCouponShareCouponList) *models.PCQkCouponShareCouponList {
	cscl := &models.PCQkCouponShareCouponList{}

	for _, item := range sscl.List {
		var nickName string
		var couponContent string

		up, errCode := passport.GetUserProfileByAccount(item.Mobile)
		if errCode == errorcode.EC_GL_SUCCESS && up.NickName != "" {
			nickName = up.NickName
		} else {
			nickName = item.Mobile[0:3] + "******" + item.Mobile[len(item.Mobile)-2:]
		}

		if item.Ctype == int32(models.EQKCOUPON_CTYPE_RECHARGE_GIVE) {
			couponContent += fmt.Sprintf("充值%d轻币送%d轻币", item.Sum/100, item.Discount/100)
		} else if item.Ctype == int32(models.EQKCOUPON_CTYPE_WASH_DISCOUNT) {
			couponContent += fmt.Sprintf("%s%.1f折", utils.Lmode2Str(item.Lmode), float32(item.Discount)/100.00)
		} else if item.Ctype == int32(models.EQKCOUPON_CTYPE_WASH_FREE) {
			couponContent += fmt.Sprintf("%s免费洗衣", utils.Lmode2Str(item.Lmode))
		}

		content := fmt.Sprintf("用户%s抢到了%s优惠劵", nickName, couponContent)
		cscl.List = append(cscl.List, content)
	}

	return cscl
}

// @router /list [get]
func (p *ShareCouponListController) ShareCouponList() {
	defer p.Output("ShareCouponList")

	errCode, _, share_id := p.verification()
	if errCode != errorcode.EC_GL_SUCCESS {
		p.Resp["code"] = errCode
		return
	}

	sscl, errCode := db.GetShareCouponList(share_id)

	p.Resp["code"] = errCode

	if errCode == errorcode.EC_GL_SUCCESS {
		p.Resp["data"] = p.sShareCouponList2CShareCouponList(sscl)
	}
}
