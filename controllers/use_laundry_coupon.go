/*
 * Copyrignt (c) doubimeizhi.com. All Rights Reserved.
 * Author: mr.gao<fbg@live.com>
 * Created Time: 2016-12-07 15:06
 * Last Modified: 2017-07-28 14:38:19
 * File Name: qkcoupon/controllers/user_laundry_coupon.go
 * Description:
 */

package controllers

import (
	"encoding/json"
	"qkcoupon/db"
	"qkcoupon/errorcode"
	"qkcoupon/rpc/client/passport"
	"qkcoupon/rpc/client/qingke"
	"qkcoupon/utils"
	"time"

	"qkcoupon/models"

	log "github.com/cihub/seelog"
)

type UseLaundryCouponController struct {
	//SCLoginedController
	URLSignController
}

/*
 *@note  验证请求参数
 *@return uint: 错误码
 */
func (p *UseLaundryCouponController) verification(lup *models.PSQkCouponLaundryUseParam, cuie *models.PSQkCouponUserInfoEx) uint {
	var errCode uint = errorcode.EC_GL_SUCCESS

	for {
		if cuie.UserInfo.Cstate == int32(models.EQKCOUPON_CSTATE_USED) {
			log.Warn("uie.UserInfo.Cstate == models.EQKCOUPON_CSTATE_USED")
			errCode = errorcode.EC_QKCOUPON_COUPON_ALREADY_USED
			break
		}

		if time.Now().Unix() > cuie.BaseInfo.EndTick {
			log.Warn("now > cuie.BaseInfo.EndTick")
			errCode = errorcode.EC_QKCOUPON_COUPON_INVALID
			break
		}

		if lup.ModeId == 0 {
			log.Warnf("uid:%s, %s", cuie.UserInfo.Uid, errorcode.Msg(errorcode.EC_GL_INVALID_URL_PARAMS))
			errCode = errorcode.EC_GL_INVALID_URL_PARAMS
			break
		}

		wi, errCode := qingke.GetWashingmodeQueryByModeId(p.GetString("ak", ""), lup.ModeId)
		if errCode != errorcode.EC_GL_SUCCESS {
			log.Warnf("uid:%s, %s", cuie.UserInfo.Uid, errorcode.Msg(errCode))
			errCode = errorcode.EC_GL_INVALID_URL_PARAMS
			break
		}

		if !utils.IsExistRwid(wi.RoomId, wi.WasherId, cuie.BaseInfo.Rwids) {
			log.Warnf("uid:%s, %s", cuie.UserInfo.Uid, "rwid not exist")
			errCode = errorcode.EC_GL_INVALID_URL_PARAMS
			break
		}

		break
	}

	return errCode
}

// @router /use [post]
func (p *UseLaundryCouponController) UseLaundryCoupon() {
	defer p.Output("UseLaundryCoupon")

	uid := p.GetString("uid")
	if uid == "" {
		log.Warn("uid==''")
		p.Resp["code"] = errorcode.EC_GL_INVALID_URL_PARAMS
		return
	}

	up, errCode := passport.GetUserProfileByUID(uid)
	if errCode != errorcode.EC_GL_SUCCESS || up.Mobile == "" {
		p.Resp["code"] = errorcode.EC_GL_INVALID_URL_PARAMS
		return
	}

	data := p.Ctx.Input.RequestBody
	if len(data) == 0 {
		log.Warn("len(data)==0")
		p.Resp["code"] = errorcode.EC_GL_INVALID_URL_PARAMS
		return
	}

	log.Debug(string(data))

	lup := &models.PSQkCouponLaundryUseParam{}
	err := json.Unmarshal(data, lup)
	if err != nil {
		log.Warn(err.Error())
		p.Resp["code"] = errorcode.EC_GL_INVALID_URL_PARAMS
		return
	}

	cuc, errCode := db.GetCoupon(uid, up.Mobile, lup.Ccode)
	if errCode != errorcode.EC_GL_SUCCESS {
		log.Warn(errorcode.Msg(errorcode.EC_QKCOUPON_COUPON_NOT_EXIST))
		p.Resp["code"] = errorcode.EC_QKCOUPON_COUPON_NOT_EXIST
		return
	}

	errCode = p.verification(lup, cuc)
	if errCode != errorcode.EC_GL_SUCCESS {
		p.Resp["code"] = errCode
		return
	}

	errCode = db.SetCouponCState(uid, up.Mobile, lup.Ccode, int32(models.EQKCOUPON_CSTATE_USED))
	if errCode != errorcode.EC_GL_SUCCESS {
		p.Resp["code"] = errCode
		return
	}

	uri := &models.PCQkCouponUseRetInfo{}
	uri.Ctype = cuc.BaseInfo.Ctype
	uri.Sum = cuc.BaseInfo.Sum
	uri.Discount = cuc.BaseInfo.Discount
	uri.MaxDiscount = cuc.BaseInfo.MaxDiscount

	p.Resp["data"] = uri
	p.Resp["code"] = errorcode.EC_GL_SUCCESS
}
