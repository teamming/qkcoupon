/*
 * Copyrignt (c) doubimeizhi.com. All Rights Reserved.
 * Author: mr.gao<fbg@live.com>
 * Created Time: 2016-12-07 15:06
 * Last Modified: 2017-07-28 14:37:03
 * File Name: qkcoupon/controllers/user_laundry_coupon.go
 * Description:
 */

package controllers

import (
	"encoding/json"
	"qkcoupon/db"
	"qkcoupon/errorcode"
	"qkcoupon/rpc/client/passport"
	"qkcoupon/rpc/client/qingke"
	"qkcoupon/utils"
	"time"

	"qkcoupon/models"

	log "github.com/cihub/seelog"
)

type UseCouponController struct {
	//	SCLoginedController
	URLSignController
}

/*
 *@note  验证请求参数
 *@return uint: 错误码
 */
func (p *UseCouponController) verification(req *models.PSQkCouponUseReq, cuie *models.PSQkCouponUserInfoEx) uint {
	var errCode uint = errorcode.EC_GL_SUCCESS

	for {
		if cuie.UserInfo.Cstate == int32(models.EQKCOUPON_CSTATE_USED) {
			log.Warn("uie.UserInfo.Cstate == models.EQKCOUPON_CSTATE_USED")
			errCode = errorcode.EC_QKCOUPON_COUPON_ALREADY_USED
			break
		}

		if time.Now().Unix() > cuie.BaseInfo.EndTick {
			log.Warn("now > cuie.BaseInfo.EndTick")
			errCode = errorcode.EC_QKCOUPON_COUPON_INVALID
			break
		}

		if req.Sum <= 0 {
			log.Warn("req.Sum <= 0")
			errCode = errorcode.EC_QKCOUPON_COUPON_INVALID
			break
		}

		if cuie.BaseInfo.Ctype == int32(models.EQKCOUPON_CTYPE_WASH_DISCOUNT) ||
			cuie.BaseInfo.Ctype == int32(models.EQKCOUPON_CTYPE_WASH_FREE) ||
			cuie.BaseInfo.Ctype == int32(models.EQKCOUPON_CTYPE_WASH_CASHCOUPON) {
			//当为洗衣折扣劵、免费洗衣劵、洗衣代金券时，判断ModeId
			if req.ModeId == 0 {
				log.Warnf("uid:%s, %s", cuie.UserInfo.Uid, errorcode.Msg(errorcode.EC_GL_INVALID_URL_PARAMS))
				errCode = errorcode.EC_GL_INVALID_URL_PARAMS
				break
			}

			wi, errCode := qingke.GetWashingmodeQueryByModeId(p.GetString("ak", ""), req.ModeId)
			if errCode != errorcode.EC_GL_SUCCESS {
				log.Warnf("uid:%s, %s", cuie.UserInfo.Uid, errorcode.Msg(errCode))
				errCode = errorcode.EC_GL_INVALID_URL_PARAMS
				break
			}

			if !utils.IsExistRwid(wi.RoomId, wi.WasherId, cuie.BaseInfo.Rwids) {
				log.Warnf("uid:%s, %s", cuie.UserInfo.Uid, "rwid not exist")
				errCode = errorcode.EC_GL_INVALID_URL_PARAMS
				break
			}
		} else {
			if req.Sum != cuie.BaseInfo.Sum {
				log.Warnf("lup.Sum:%d != cuie.BaseInfo.Sum:%d", req.Sum, cuie.BaseInfo.Sum)
				errCode = errorcode.EC_GL_INVALID_URL_PARAMS
				break
			}
		}

		break
	}

	return errCode
}

// @router /use [post]
func (p *UseCouponController) UseCoupon() {
	defer p.Output("UseCoupon")

	uid := p.GetString("uid")
	if uid == "" {
		log.Warn("uid==''")
		p.Resp["code"] = errorcode.EC_GL_INVALID_URL_PARAMS
		return
	}

	up, errCode := passport.GetUserProfileByUID(uid)
	if errCode != errorcode.EC_GL_SUCCESS || up.Mobile == "" {
		p.Resp["code"] = errorcode.EC_GL_INVALID_URL_PARAMS
		return
	}

	data := p.Ctx.Input.RequestBody
	if len(data) == 0 {
		log.Warn("len(data)==0")
		p.Resp["code"] = errorcode.EC_GL_INVALID_URL_PARAMS
		return
	}

	log.Debug(string(data))

	req := &models.PSQkCouponUseReq{}
	err := json.Unmarshal(data, req)
	if err != nil {
		log.Warn(err.Error())
		p.Resp["code"] = errorcode.EC_GL_INVALID_URL_PARAMS
		return
	}

		cuie, errCode := db.GetCoupon(uid, up.Mobile, req.Ccode)
		if errCode != errorcode.EC_GL_SUCCESS {
			log.Warn(errorcode.Msg(errorcode.EC_QKCOUPON_COUPON_NOT_EXIST))
			p.Resp["code"] = errorcode.EC_QKCOUPON_COUPON_NOT_EXIST
			return
		}
	if req.Flag=="using"||req.Flag=="" {
		errCode = p.verification(req, cuie)
		if errCode != errorcode.EC_GL_SUCCESS {
			p.Resp["code"] = errCode
			return
		}
	}
        if req.Flag=="used"||req.Flag=="" {
		errCode = db.SetCouponCState(uid, up.Mobile, req.Ccode, int32(models.EQKCOUPON_CSTATE_USED))
		if errCode != errorcode.EC_GL_SUCCESS {
			p.Resp["code"] = errCode
			return
		}
	}
	rsp := &models.PSQkCouponUseRsp{}

	if cuie.BaseInfo.Ctype == int32(models.EQKCOUPON_CTYPE_RECHARGE_GIVE) {
		rsp.PayCoin = req.Sum
		rsp.CalcResult = req.Sum + cuie.BaseInfo.Discount
	} else if cuie.BaseInfo.Ctype == int32(models.EQKCOUPON_CTYPE_WASH_DISCOUNT) {
		calcRet := float32(req.Sum) * (float32(cuie.BaseInfo.Discount) / 1000.00)
		//解决当金额小于0.01时，还是显示0.01
		if calcRet < 1 {
			calcRet = 1
		}
		rsp.PayCoin = int32(calcRet)
		rsp.CalcResult = int32(calcRet)
	} else if cuie.BaseInfo.Ctype == int32(models.EQKCOUPON_CTYPE_WASH_FREE) {
		rsp.PayCoin = 0
		rsp.CalcResult = 0
	} else if cuie.BaseInfo.Ctype == int32(models.EQKCOUPON_CTYPE_RECHARGE_CASHCOUPON) {
		rsp.PayCoin = req.Sum - cuie.BaseInfo.Discount
		rsp.CalcResult = req.Sum
	} else if cuie.BaseInfo.Ctype == int32(models.EQKCOUPON_CTYPE_WASH_CASHCOUPON) {
		calcRet := req.Sum - cuie.BaseInfo.Discount
		rsp.PayCoin = calcRet
		rsp.CalcResult = calcRet
	}

	if rsp.PayCoin < 0 {
		rsp.PayCoin = 0
	}

	if rsp.CalcResult < 0 {
		rsp.CalcResult = 0
	}

	log.Debug(rsp)

	p.Resp["code"] = errorcode.EC_GL_SUCCESS
	p.Resp["data"] = rsp
}
