/*
 * Copyrignt (c) doubimeizhi.com. All Rights Reserved.
 * Author: mr.gao<fbg@live.com>
 * Created Time: 2016-12-07 15:06
 * Last Modified: 2017-07-28 14:38:37
 * File Name: qkcoupon/controllers/dels_coupon.go
 * Description:
 */

package controllers

import (
	"encoding/json"
	"qkcoupon/db"
	"qkcoupon/errorcode"
	"qkcoupon/models"
	"qkcoupon/rpc/client/passport"

	log "github.com/cihub/seelog"
)

type DelsCouponController struct {
	LoginedController
}

// @router /dels [post]
func (p *DelsCouponController) DelsCoupon() {
	defer p.Output("DelsCoupon")

	uid := p.GetString("uid")
	if uid == "" {
		log.Warn("uid == ''")
		p.Resp["code"] = errorcode.EC_GL_INVALID_URL_PARAMS
		return
	}

	up, errCode := passport.GetUserProfileByUID(uid)
	if errCode != errorcode.EC_GL_SUCCESS || up.Mobile == "" {
		p.Resp["code"] = errCode
		return
	}

	data := p.Ctx.Input.RequestBody
	if len(data) == 0 {
		log.Warn("len(data)==0")
		p.Resp["code"] = errorcode.EC_GL_INVALID_URL_PARAMS
		return
	}

	//log.Debug(string(data))

	dcp := &models.PCQkCouponDelsParam{}
	err := json.Unmarshal(data, dcp)
	if err != nil {
		log.Warn("Unmarshal failed!")
		p.Resp["code"] = errorcode.EC_GL_INVALID_URL_PARAMS
		return
	}

	//log.Debug("dcp:", dcp)

	//校验参数

	p.Resp["code"] = db.DelCouponList(uid, up.Mobile, dcp.Ccodes)
}
