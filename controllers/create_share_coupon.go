/*
 * Copyrignt (c) doubimeizhi.com. All Rights Reserved.
 * Author: mr.gao<fbg@live.com>
 * Created Time: 2016-12-05 09:48
 * Last Modified: 2017-07-28 09:46:40
 * File Name: qkcoupon/controllers/create_share_coupon.go
 * Description:
 */

package controllers

import (
	"encoding/json"

	"qkcoupon/db"
	"qkcoupon/errorcode"
	"qkcoupon/models"
	"qkcoupon/utils"

	log "github.com/cihub/seelog"
)

type CreateShareCouponController struct {
	//	SCLoginedController
	URLSignController
}

/*
 *@note  验证请求参数
 *param ccp: *TCreateCouponParam结构
 *@return (int64, int64, uint): (开始时间, 结束时间, 错误码)
 */
func (p *CreateShareCouponController) verification(cscp *models.PSQkCouponCreateShareCouponParam) (uint, string) {
	var errCode uint = errorcode.EC_GL_SUCCESS
	var uid string = ""

	for {
		uid = p.GetString("uid")
		if uid == "" {
			log.Warn("uid==''")
			errCode = errorcode.EC_GL_INVALID_URL_PARAMS
			break
		}

		if !utils.CheckOperationType(cscp.OperationType) {
			log.Warnf("operation type:%d", cscp.OperationType)
			errCode = errorcode.EC_GL_INVALID_URL_PARAMS
			break
		}

		break
	}

	return errCode, uid
}

func (p *CreateShareCouponController) createRule(cscp *models.PSQkCouponCreateShareCouponParam) *models.PSQkCouponShareCouponRuleList {
	scrl := &models.PSQkCouponShareCouponRuleList{}

	ri, ok := models.ShareRuleMap[cscp.OperationType]
	if !ok {
		return nil
	}

	for _, v := range ri {
		if v.Ctype == int32(models.EQKCOUPON_CTYPE_RECHARGE_GIVE) {
			scrl.List = append(scrl.List, p.createRechargeCoupon(v.Sum, v.Discount, v.Count, v.ExpiryDay))
		} else if v.Ctype == int32(models.EQKCOUPON_CTYPE_WASH_DISCOUNT) {
			scrl.List = append(scrl.List, p.createDiscountCoupon(v.Discount, v.Count, v.ExpiryDay))
		} else if v.Ctype == int32(models.EQKCOUPON_CTYPE_RECHARGE_CASHCOUPON) {
			scrl.List = append(scrl.List, p.createRechargeCashCoupon(v.Sum, v.Discount, v.Count, v.ExpiryDay))
		} else if v.Ctype == int32(models.EQKCOUPON_CTYPE_WASH_CASHCOUPON) {
			scrl.List = append(scrl.List, p.createWashCashCoupon(v.Discount, v.Count, v.ExpiryDay))
		} else {
			scrl.List = append(scrl.List, p.createFreeCoupon(v.Count, v.ExpiryDay))
		}

		scrl.Count += v.Count
	}

	return scrl
}

func (p *CreateShareCouponController) createFreeCoupon(count int32, expiryDay int32) *models.PSQkCouponShareCouponRule {
	scr := &models.PSQkCouponShareCouponRule{}
	scr.Ctype = int32(models.EQKCOUPON_CTYPE_WASH_FREE)
	scr.Count = count
	scr.ExpiryDay = expiryDay
	return scr
}

func (p *CreateShareCouponController) createRechargeCoupon(sum int32, discount int32, count int32, expiryDay int32) *models.PSQkCouponShareCouponRule {
	scr := &models.PSQkCouponShareCouponRule{}
	scr.Ctype = int32(models.EQKCOUPON_CTYPE_RECHARGE_GIVE)
	scr.Sum = sum
	scr.Discount = discount
	scr.Count = count
	scr.ExpiryDay = expiryDay
	return scr
}

func (p *CreateShareCouponController) createDiscountCoupon(discount int32, count int32, expiryDay int32) *models.PSQkCouponShareCouponRule {
	scr := &models.PSQkCouponShareCouponRule{}
	scr.Ctype = int32(models.EQKCOUPON_CTYPE_WASH_DISCOUNT)
	scr.Discount = discount
	scr.Count = count
	scr.ExpiryDay = expiryDay
	return scr
}

func (p *CreateShareCouponController) createRechargeCashCoupon(sum int32, discount int32, count int32, expiryDay int32) *models.PSQkCouponShareCouponRule {
	scr := &models.PSQkCouponShareCouponRule{}
	scr.Ctype = int32(models.EQKCOUPON_CTYPE_RECHARGE_CASHCOUPON)
	scr.Sum = sum
	scr.Discount = discount
	scr.Count = count
	scr.ExpiryDay = expiryDay
	return scr
}

func (p *CreateShareCouponController) createWashCashCoupon(discount int32, count int32, expiryDay int32) *models.PSQkCouponShareCouponRule {
	scr := &models.PSQkCouponShareCouponRule{}
	scr.Ctype = int32(models.EQKCOUPON_CTYPE_WASH_CASHCOUPON)
	scr.Discount = discount
	scr.Count = count
	scr.ExpiryDay = expiryDay
	return scr
}

// @router /create [post]
func (p *CreateShareCouponController) CreateShareCoupon() {
	defer p.Output("CreateShareCoupon")

	data := p.Ctx.Input.RequestBody
	if len(data) == 0 {
		log.Warn("len(data)==0")
		p.Resp["code"] = errorcode.EC_GL_INVALID_URL_PARAMS
		return
	}

	log.Debug(string(data))

	cscp := &models.PSQkCouponCreateShareCouponParam{}
	err := json.Unmarshal(data, cscp)
	if err != nil {
		log.Warnf("%s", err.Error())
		p.Resp["code"] = errorcode.EC_GL_INVALID_URL_PARAMS
		return
	}

	log.Debug(cscp)

	errCode, uid := p.verification(cscp)
	if errCode != errorcode.EC_GL_SUCCESS {
		p.Resp["code"] = errCode
		return
	}

	errCode = db.IsExistShareCoupon(cscp.ShareId)
	if errCode != errorcode.EC_QKCOUPON_COUPON_NOT_EXIST {
		p.Resp["code"] = errCode
		return
	}

	scrl := p.createRule(cscp)
	if scrl == nil {
		p.Resp["code"] = errorcode.EC_GL_INVALID_URL_PARAMS
		return
	}

	errCode = db.CreateShareCoupon(uid, cscp, scrl)
	if errCode != errorcode.EC_GL_SUCCESS {
		p.Resp["code"] = errCode
		return
	}

	p.Resp["code"] = errorcode.EC_GL_SUCCESS
}
