/*
 * Copyrignt (c) doubimeizhi.com. All Rights Reserved.
 * Author: mr.gao<fbg@live.com>
 * Created Time: 2016-12-07 15:06
 * Last Modified: 2017-07-28 14:35:08
 * File Name: qkcoupon/controllers/use_recharge_coupon.go
 * Description:
 */

package controllers

import (
	"encoding/json"
	"qkcoupon/db"
	"qkcoupon/errorcode"
	"qkcoupon/models"
	"qkcoupon/rpc/client/passport"
	"time"

	log "github.com/cihub/seelog"
)

type UseRechargeCouponController struct {
	//	SCLoginedController
	URLSignController
}

/*
 *@note  验证请求参数
 *@return (uint, string): (错误码, uid)
 */
func (p *UseRechargeCouponController) verification(rup *models.PSQkCouponRechargeUseParam, uie *models.PSQkCouponUserInfoEx) uint {
	var errCode uint = errorcode.EC_GL_SUCCESS

	for {
		if uie.UserInfo.Cstate == int32(models.EQKCOUPON_CSTATE_USED) {
			log.Warn("uie.UserInfo.Cstate == models.EQKCOUPON_CSTATE_USED")
			errCode = errorcode.EC_QKCOUPON_COUPON_ALREADY_USED
			break
		}

		if time.Now().Unix() > uie.BaseInfo.EndTick {
			log.Warn("now > uie.BaseInfo.EndTick")
			errCode = errorcode.EC_QKCOUPON_COUPON_INVALID
			break
		}

		if rup.Sum != uie.BaseInfo.Sum {
			log.Warnf("ucp.Sum:%d < cuc.BaseInfo.Sum:%d", rup.Sum, uie.BaseInfo.Sum)
			errCode = errorcode.EC_GL_INVALID_URL_PARAMS
			break
		}

		break
	}

	return errCode
}

// @router /use [post]
func (p *UseRechargeCouponController) UseRechargeCoupon() {
	defer p.Output("UseRechargeCoupon")

	uid := p.GetString("uid")
	if uid == "" {
		log.Warn("uid==''")
		p.Resp["code"] = errorcode.EC_GL_INVALID_URL_PARAMS
		return
	}

	up, errCode := passport.GetUserProfileByUID(uid)
	if errCode != errorcode.EC_GL_SUCCESS || up.Mobile == "" {
		p.Resp["code"] = errCode
		return
	}

	data := p.Ctx.Input.RequestBody
	if len(data) == 0 {
		log.Warn("len(data) == 0")
		p.Resp["code"] = errorcode.EC_GL_INVALID_URL_PARAMS
		return
	}

	log.Debug(string(data))

	rup := &models.PSQkCouponRechargeUseParam{}
	err := json.Unmarshal(data, rup)
	if err != nil {
		log.Warn("Unmarshal failed!")
		p.Resp["code"] = errorcode.EC_GL_INVALID_URL_PARAMS
		return
	}

	log.Debug(rup)

	uie, errCode := db.GetCoupon(uid, up.Mobile, rup.Ccode)
	if errCode != errorcode.EC_GL_SUCCESS {
		log.Warn(errorcode.Msg(errorcode.EC_QKCOUPON_COUPON_NOT_EXIST))
		p.Resp["code"] = errorcode.EC_QKCOUPON_COUPON_NOT_EXIST
		return
	}

	errCode = p.verification(rup, uie)
	if errCode != errorcode.EC_GL_SUCCESS {
		p.Resp["code"] = errCode
		return
	}

	errCode = db.SetCouponCState(uid, up.Mobile, rup.Ccode, int32(models.EQKCOUPON_CSTATE_USED))
	if errCode != errorcode.EC_GL_SUCCESS {
		p.Resp["code"] = errCode
		return
	}

	uri := &models.PCQkCouponUseRetInfo{}
	uri.Ctype = uie.BaseInfo.Ctype
	uri.Sum = uie.BaseInfo.Sum
	uri.Discount = uie.BaseInfo.Discount
	uri.MaxDiscount = uie.BaseInfo.MaxDiscount

	p.Resp["data"] = uri
	p.Resp["code"] = errorcode.EC_GL_SUCCESS
}
