package controllers

import (
	"strconv"
	"time"

	"qkcoupon/db"
	"qkcoupon/errorcode"
	"qkcoupon/models"

	log "github.com/cihub/seelog"
)

type Event201705Controller struct {
	URLSignController
}

func (p *Event201705Controller) NewUser() {
	defer p.Output("Event201705")

	mobile := p.GetString("mobile")
	mobileCheck, mobileErr := strconv.ParseInt(mobile, 10, 64)
	if mobileErr != nil {
		log.Infof("非法手机号%d", mobile)
		p.Resp["code"] = errorcode.EC_GL_MOBILE_FORMAT_ERROR
		return
	}
	if mobileCheck >= 19000000000 || mobileCheck <= 12000000000 {
		log.Infof("非法手机号%d", mobile)
		p.Resp["code"] = errorcode.EC_GL_MOBILE_FORMAT_ERROR
		return
	}

	origin, _ := p.GetInt("origin", 1)
	if origin != 1 && origin != 2 {
		origin = 1
	}

	/*isNewUser, errCheck := db.CheckNewUserCoupon(mobile)
	if errCheck != errorcode.EC_GL_SUCCESS {
		p.Resp["code"] = errCheck
		return
	}
	if !isNewUser {
		p.Resp["code"] = errorcode.EC_QKCOUPON_ALREADY_RECEIVE_COUPON
		return
	}*/

	loc, _ := time.LoadLocation("Local")
	timeFormat := "2006.01.02"
	startTime := time.Now().Format(timeFormat)
	endTime := time.Now().Add(time.Hour * 24 * 5).Format(timeFormat)
	startTick, _ := time.ParseInLocation(timeFormat, startTime, loc)
	endTick, _ := time.ParseInLocation(timeFormat, endTime, loc)
	endTick = endTick.Add(-time.Second)

	cc := &models.PSQkCouponCreateCouponReq{}
	cc.Ctype = 3
	cc.Lmode = 0
	cc.Utype = 1
	cid, err := db.CreateCoupon(cc, startTick.Unix(), endTick.Unix(), 0)
	if err != errorcode.EC_GL_SUCCESS {
		p.Resp["code"] = err
		return
	}
	log.Debugf("创建新用户活动优惠卷，cid：%d，手机号：%s", cid, mobile)

	ccode, err2 := db.DispatchCouponByMobile(cid, mobile)
	if err2 != errorcode.EC_GL_SUCCESS {
		p.Resp["code"] = err2
		return
	}
	log.Debugf("创建新用户活动优惠卷，ccode：%s，手机号：%s", ccode, mobile)

	err3 := db.RecordNewUserCoupon(ccode, mobile, origin)
	if err3 != errorcode.EC_GL_SUCCESS {
		p.Resp["code"] = err3
		return
	}

	p.Resp["code"] = errorcode.EC_GL_SUCCESS
}

func (p *Event201705Controller) NewUser6() {
	defer p.Output("Event201705")

	mobile := p.GetString("mobile")
	mobileCheck, mobileErr := strconv.ParseInt(mobile, 10, 64)
	if mobileErr != nil {
		log.Infof("非法手机号%d", mobile)
		p.Resp["code"] = errorcode.EC_GL_MOBILE_FORMAT_ERROR
		return
	}
	if mobileCheck >= 19000000000 || mobileCheck <= 12000000000 {
		log.Infof("非法手机号%d", mobile)
		p.Resp["code"] = errorcode.EC_GL_MOBILE_FORMAT_ERROR
		return
	}

	isNewUser, errCheck := db.CheckNewUserCoupon6(mobile)
	if errCheck != errorcode.EC_GL_SUCCESS {
		p.Resp["code"] = errCheck
		return
	}
	if !isNewUser {
		p.Resp["code"] = errorcode.EC_QKCOUPON_ALREADY_RECEIVE_COUPON
		return
	}

	now := time.Now().Unix()
	endTick := now - now%86400 - 28800 - 1

	for i := 0; i < 6; i++ {
		startTick := endTick + 1
		endTick += 86400 * 2
		if i == 0 {
			endTick += 86400
		}
		cc := &models.PSQkCouponCreateCouponReq{}
		cc.Ctype = 3
		cc.Lmode = 0
		cc.Utype = 1
		cid, err := db.CreateCoupon(cc, startTick, endTick, 0)
		if err != errorcode.EC_GL_SUCCESS {
			p.Resp["code"] = err
			return
		}
		log.Debugf("创建新用户活动优惠卷，cid：%d，手机号：%s，第%d张", cid, mobile, i+1)

		ccode, err2 := db.DispatchCouponByMobile(cid, mobile)
		if err2 != errorcode.EC_GL_SUCCESS {
			p.Resp["code"] = err2
			return
		}
		log.Debugf("创建新用户活动优惠卷，ccode：%s，手机号：%s，第%d张", ccode, mobile, i+1)

		err3 := db.RecordNewUserCoupon6(ccode, mobile, i+1)
		if err3 != errorcode.EC_GL_SUCCESS {
			p.Resp["code"] = err3
			return
		}
	}

	p.Resp["code"] = errorcode.EC_GL_SUCCESS
}
