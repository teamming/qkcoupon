/*
 * Copyrignt (c) doubimeizhi.com. All Rights Reserved.
 * Author: eason.gao<fbg@live.com>
 * Created Time: 2017-02-16 15:06
 * Last Modified: 2017-07-28 14:35:28
 * File Name: qkcoupon/controllers/receive_coupon.go
 * Description:
 */

package controllers

import (
	"qkcoupon/db"
	"qkcoupon/errorcode"
	"qkcoupon/models"
	"qkcoupon/rpc/client/passport"

	log "github.com/cihub/seelog"
)

type FirstWashingController struct {
	//	SCLoginedController
	URLSignController
}

/*
 *@note  验证请求参数
 *@return (string, uint): (uid, 错误码)
 */
func (p *FirstWashingController) verification() (string, uint) {
	var errCode uint = errorcode.EC_GL_SUCCESS
	var mobile string = ""

	for {
		uid := p.GetString("uid", "")
		if uid == "" {
			log.Warn("uid == ''")
			errCode = errorcode.EC_GL_INVALID_URL_PARAMS
			break
		}

		var up *models.PQSQkCouponUserProfile
		up, errCode = passport.GetUserProfileByUID(uid)
		if errCode != errorcode.EC_GL_SUCCESS {
			break
		}

		mobile = up.Mobile

		break
	}

	return mobile, errCode
}

// @router /first_washing [post]
func (p *FirstWashingController) FirstWashing() {
	defer p.Output("FirstWashing")

	mobile, errCode := p.verification()
	if errCode != errorcode.EC_GL_SUCCESS {
		p.Resp["code"] = errCode
		return
	}

	rri, errCode := db.GetReceiveRecommendInfo(mobile)
	if errCode != errorcode.EC_GL_SUCCESS {
		p.Resp["code"] = errCode
		return
	}

	if rri.Return {
		p.Resp["code"] = errorcode.EC_QKCOUPON_ALREADY_CALL_FIRST_WASHING
		return
	}

	_, errCode = db.ReceiveRecommend(rri.RecommendId, mobile, models.EQKCOUPON_OPERATOR_RECOMMENDER)
	if errCode != errorcode.EC_GL_SUCCESS {
		p.Resp["code"] = errCode
		return
	}

	p.Resp["code"] = errorcode.EC_GL_SUCCESS
}
