/*
 * Copyrignt (c) doubimeizhi.com. All Rights Reserved.
 * Author: mr.gao<fbg@live.com>
 * Created Time: 2016-12-07 15:06
 * Last Modified: 2017-07-28 14:34:14
 * File Name: qkcoupon/controllers/recharge_coupon_list.go
 * Description:
 */

package controllers

import (
	"qkcoupon/db"
	"qkcoupon/errorcode"
	"qkcoupon/rpc/client/passport"
	"qkcoupon/utils"
	"time"

	"qkcoupon/models"

	log "github.com/cihub/seelog"
)

type RechargeCouponListController struct {
	LoginedController
}

/*
 *@note  验证请求参数
 *@return (uint, string, int32, int8, int32): (错误码, uid, sum, usable, version)
 */
func (p *RechargeCouponListController) verification() (uint, string, int32, int8, int32) {
	var errCode uint = errorcode.EC_GL_SUCCESS
	var uid string = ""
	var sum int32 = 0
	var usable int8 = 0
	var version int32 = 0

	for {
		var err error

		uid = p.GetString("uid", "")
		if uid == "" {
			log.Warn("uid == ''")
			errCode = errorcode.EC_GL_INVALID_URL_PARAMS
			break
		}

		sum, err = p.GetInt32("sum", 0)
		if err != nil {
			log.Warn("sum not exist")
			errCode = errorcode.EC_GL_INVALID_URL_PARAMS
			break
		}

		usable, err = p.GetInt8("usable", 0)
		if err != nil {
			log.Warn("usable not exist")
			errCode = errorcode.EC_GL_INVALID_URL_PARAMS
			break
		}

		version, _ = p.GetInt32("version", 0)

		break
	}

	return errCode, uid, sum, usable, version
}

/*
 *@note 服务端数据转客户端数据
 *@param sum: 轻币值
 *@param usable: 可用与不可用
 *@param cucsl: *PSQkCouponUserInfoExList
 *@param version: 版本号
 *@return *PCQkCouponRechargeInfoList
 */
func (p *RechargeCouponListController) s2c(sum int32, usable int8, cucsl *models.PSQkCouponUserInfoExList, version int32) *models.PCQkCouponRechargeInfoList {
	ril := &models.PCQkCouponRechargeInfoList{}
	ril.List = make([]*models.PCQkCouponRechargeInfo, 0)

	now := time.Now().Unix()
	var isTimeRange bool

	for _, cucs := range cucsl.List {
		//客户端版本小于为6,而且为代金劵，不返回
		if cucs.BaseInfo.Ctype == int32(models.EQKCOUPON_CTYPE_RECHARGE_CASHCOUPON) ||
			cucs.BaseInfo.Ctype == int32(models.EQKCOUPON_CTYPE_WASH_CASHCOUPON) {
			if version < 6 {
				continue
			}
		}

		//获取是否当前时间在可用的范围内的状态
		if cucs.BaseInfo.TimeRanges != nil {
			isTimeRange = utils.CheckGetTimeRange(cucs.BaseInfo.TimeRanges)
		} else {
			isTimeRange = utils.CheckStartTime2EndTime(cucs.BaseInfo.StartTick, cucs.BaseInfo.EndTick)
		}

		if cucs.UserInfo.Cstate != int32(models.EQKCOUPON_CSTATE_NOTUSED) {
			continue
		}

		if usable == 1 {
			//以下是可用页面显示逻辑
			if !isTimeRange {
				continue
			}

			//不是充值优惠劵的
			if cucs.BaseInfo.Ctype != int32(models.EQKCOUPON_CTYPE_RECHARGE_GIVE) &&
				cucs.BaseInfo.Ctype != int32(models.EQKCOUPON_CTYPE_RECHARGE_CASHCOUPON) {
				continue
			}

			//不等于sum的
			if cucs.BaseInfo.Sum != sum {
				continue
			}
		} else {
			//以下是不可用页面显示逻辑
			var ok bool

			//不是充值优惠劵，而且未过期的
			if (cucs.BaseInfo.Ctype != int32(models.EQKCOUPON_CTYPE_RECHARGE_GIVE) &&
				cucs.BaseInfo.Ctype != int32(models.EQKCOUPON_CTYPE_RECHARGE_CASHCOUPON)) &&
				cucs.BaseInfo.EndTick > now {
				ok = true
			}

			//是充值优惠劵，而且未过期的
			if (cucs.BaseInfo.Ctype == int32(models.EQKCOUPON_CTYPE_RECHARGE_GIVE) ||
				cucs.BaseInfo.Ctype == int32(models.EQKCOUPON_CTYPE_RECHARGE_CASHCOUPON)) &&
				cucs.BaseInfo.EndTick > now {
				if cucs.BaseInfo.Sum != sum {
					//不等于sum的
					ok = true
				} else if !isTimeRange {
					//在时间范围内的
					ok = true
				}
			}

			if !ok {
				continue
			}
		}

		ri := &models.PCQkCouponRechargeInfo{}
		ri.Ctype = cucs.BaseInfo.Ctype
		ri.Lmode = cucs.BaseInfo.Lmode
		ri.Ccode = cucs.UserInfo.Ccode
		ri.Sum = cucs.BaseInfo.Sum
		ri.Discount = cucs.BaseInfo.Discount
		ri.MaxDiscount = cucs.BaseInfo.MaxDiscount
		ri.Source = utils.Utype2Str(cucs.BaseInfo.Utype)

		if cucs.BaseInfo.TimeRanges != nil {
			ri.TimeRange = utils.GetClientShowDateTime(cucs.BaseInfo.TimeRanges)
		} else {
			ri.TimeRange = utils.GetClientShowDateTime2(cucs.BaseInfo.StartTick, cucs.BaseInfo.EndTick)
		}
		token := p.GetString("ak", "")
		uid := p.GetString("uid", "")
		ri.UseRange = utils.GetUseRange(cucs.BaseInfo.Rwids, token, uid)
		ril.List = append(ril.List, ri)
	}

	return ril
}

// @router /list [get]
func (p *RechargeCouponListController) RechargeCouponList() {
	defer p.Output("RechargeCouponList")

	errCode, uid, sum, usable, version := p.verification()
	if errCode != errorcode.EC_GL_SUCCESS {
		p.Resp["code"] = errCode
		return
	}

	up, errCode := passport.GetUserProfileByUID(uid)
	if errCode != errorcode.EC_GL_SUCCESS || up.Mobile == "" {
		p.Resp["code"] = errorcode.EC_GL_INVALID_URL_PARAMS
		return
	}

	cucsl, errCode := db.GetCouponListByUid(uid, up.Mobile)

	p.Resp["code"] = errCode

	if errCode == errorcode.EC_GL_SUCCESS {
		p.Resp["data"] = p.s2c(sum, usable, cucsl, version)
	}
}
