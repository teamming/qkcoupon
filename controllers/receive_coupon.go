/*
 * Copyrignt (c) doubimeizhi.com. All Rights Reserved.
 * Author: mr.gao<fbg@live.com>
 * Created Time: 2017-02-16 15:06
 * Last Modified: 2017-02-16 15:06
 * File Name: qkcoupon/controllers/receive_coupon.go
 * Description:
 */

package controllers

import (
	"encoding/json"
	"qkcoupon/db"
	"qkcoupon/errorcode"

	"qkcoupon/models"

	"doubimeizhi.com/utility"
	log "github.com/cihub/seelog"
)

type ReceiveCouponController struct {
	URLSignController
}

/*
 *@note  验证请求参数
 *@return uint: 错误码
 */
func (p *ReceiveCouponController) verification(rc *models.PCQkCouponReceiveCouponReq) uint {
	var errCode uint = errorcode.EC_GL_SUCCESS

	for {
		//验证Mobile
		if !utility.ValidateMobile(rc.Mobile) {
			errCode = errorcode.EC_GL_INVALID_URL_PARAMS
			break
		}

		break
	}

	return errCode
}

func (p *ReceiveCouponController) userInfoEx2ReceiveCouponRsp(mobile string, uie *models.PSQkCouponUserInfoEx) *models.PCQkCouponReceiveCouponRsp {
	rcr := &models.PCQkCouponReceiveCoupon{}
	rcr.Name = mobile
	rcr.Ctype = uie.BaseInfo.Ctype
	rcr.Lmode = uie.BaseInfo.Lmode
	rcr.Sum = uie.BaseInfo.Sum
	rcr.Discount = uie.BaseInfo.Discount

	/*rcr.Desc = "恭喜你获得"

	if uie.BaseInfo.Ctype == int32(models.EQKCOUPON_CTYPE_RECHARGE) {
		rcr.Desc += fmt.Sprintf("充值%d轻币送%d轻币", uie.BaseInfo.Sum/100, uie.BaseInfo.Discount/100)
	} else if uie.BaseInfo.Ctype == int32(models.EQKCOUPON_CTYPE_DISCOUNT) {
		rcr.Desc += fmt.Sprintf("%s%.1f折", utils.Lmode2Str(uie.BaseInfo.Lmode), float32(uie.BaseInfo.Discount)/100.00)
	} else if uie.BaseInfo.Ctype == int32(models.EQKCOUPON_CTYPE_FREE) {
		rcr.Desc += fmt.Sprintf("%s免费洗衣", utils.Lmode2Str(uie.BaseInfo.Lmode))
	}

	rcr.Desc += "优惠劵"*/

	rcrl := &models.PCQkCouponReceiveCouponRsp{}
	rcrl.List = append(rcrl.List, rcr)

	return rcrl
}

// @router /receive [post]
func (p *ReceiveCouponController) ReceiveCoupon() {
	defer p.Output("ReceiveCoupon")

	data := p.Ctx.Input.RequestBody
	if len(data) == 0 {
		log.Warn("len(data)==0")
		p.Resp["code"] = errorcode.EC_GL_INVALID_URL_PARAMS
		return
	}

	log.Debug(string(data))

	req := &models.PCQkCouponReceiveCouponReq{}
	err := json.Unmarshal(data, req)
	if err != nil {
		log.Warn(err.Error())
		p.Resp["code"] = errorcode.EC_GL_INVALID_URL_PARAMS
		return
	}

	errCode := p.verification(req)
	if errCode != errorcode.EC_GL_SUCCESS {
		p.Resp["code"] = errCode
		return
	}

	errCode = db.IsExistShareCoupon(req.ShareId)
	if errCode != errorcode.EC_QKCOUPON_ALREADY_EXIST_SHARE {
		p.Resp["code"] = errCode
		return
	}

	ccode, errCode := db.IsReceiveShareCoupon(req.ShareId, req.Mobile)
	if errCode != errorcode.EC_GL_SUCCESS {
		if errCode != errorcode.EC_QKCOUPON_ALREADY_RECEIVE_COUPON {
			p.Resp["code"] = errCode
			return
		}

		uie, errCode := db.GetCoupon("", req.Mobile, ccode)
		if errCode != errorcode.EC_GL_SUCCESS {
			p.Resp["code"] = errCode
			return
		}

		log.Debugf("PSQkCouponUserInfoEx:%v", uie)

		p.Resp["data"] = p.userInfoEx2ReceiveCouponRsp(req.Mobile, uie)
		p.Resp["code"] = errorcode.EC_QKCOUPON_ALREADY_RECEIVE_COUPON
		return
	}

	errCode = db.IsGrabEnd(req.ShareId)
	if errCode != errorcode.EC_GL_SUCCESS {
		p.Resp["code"] = errCode
		return
	}

	ccode, errCode = db.ReceiveCoupon(req)
	if errCode != errorcode.EC_GL_SUCCESS {
		p.Resp["code"] = errCode
		return
	}

	uie, errCode := db.GetCoupon("", req.Mobile, ccode)
	if errCode != errorcode.EC_GL_SUCCESS {
		p.Resp["code"] = errCode
		return
	}

	//log.Debugf("PSQkCouponUserInfoEx:%v", uie)

	p.Resp["data"] = p.userInfoEx2ReceiveCouponRsp(req.Mobile, uie)
	p.Resp["code"] = errorcode.EC_GL_SUCCESS
}
