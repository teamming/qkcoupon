/*
 * Copyrignt (c) doubimeizhi.com. All Rights Reserved.
 * Author: mr.gao<fbg@live.com>
 * Created Time: 2016-12-01 18:00
 * Last Modified: 2016-12-01 18:00
 * File Name: qkcoupon/controllers/default.go
 * Description:
 */

package controllers

import (
	"github.com/astaxie/beego"
)

type DefaultController struct {
	beego.Controller
}

func (p *DefaultController) Get() {
	p.Ctx.Redirect(302, "http://dl.app.qkier.com/fingertip/?from=qingke")
}
