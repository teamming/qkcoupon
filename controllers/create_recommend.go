/*
 * Copyrignt (c) doubimeizhi.com. All Rights Reserved.
 * Author: mr.gao<fbg@live.com>
 * Created Time: 2016-12-05 09:48
 * Last Modified: 2016-12-05 09:48
 * File Name: qkcoupon/controllers/create_share_coupon.go
 * Description:
 */

package controllers

import (
	"qkcoupon/db"
	"qkcoupon/errorcode"
	"qkcoupon/models"

	log "github.com/cihub/seelog"
)

type CreateRecommendController struct {
	LoginedController
}

/*
 *@note  验证请求参数
 *@return (string, int64): (推荐者ID, 错误码)
 */
func (p *CreateRecommendController) verification() (string, uint) {
	var errCode uint = errorcode.EC_GL_SUCCESS
	var uid string = ""

	for {
		uid = p.GetString("uid")
		if uid == "" {
			log.Warn("uid==''")
			errCode = errorcode.EC_GL_INVALID_URL_PARAMS
			break
		}

		break
	}

	return uid, errCode
}

func (p *CreateRecommendController) createRule(operator models.EQKCOUPON_OPERATOR) *models.PSQkCouponRecommendRuleList {
	ri, ok := models.RecommendRuleMap[int32(operator)]
	if !ok {
		return nil
	}

	rrl := &models.PSQkCouponRecommendRuleList{}

	for _, v := range ri {
		if v.Ctype == int32(models.EQKCOUPON_CTYPE_RECHARGE_GIVE) {
			rrl.List = append(rrl.List, p.createRechargeCoupon(v.Sum, v.Discount, v.ExpiryDay))
		} else if v.Ctype == int32(models.EQKCOUPON_CTYPE_WASH_DISCOUNT) {
			rrl.List = append(rrl.List, p.createDiscountCoupon(v.Discount, v.ExpiryDay))
		} else {
			rrl.List = append(rrl.List, p.createFreeCoupon(v.ExpiryDay))
		}
	}

	log.Debug(rrl)

	return rrl
}

func (p *CreateRecommendController) createFreeCoupon(expiryDay int32) *models.PSQkCouponRecommendRule {
	rr := &models.PSQkCouponRecommendRule{}
	rr.Ctype = int32(models.EQKCOUPON_CTYPE_WASH_FREE)
	rr.ExpiryDay = expiryDay
	return rr
}

func (p *CreateRecommendController) createRechargeCoupon(sum int32, discount int32, expiryDay int32) *models.PSQkCouponRecommendRule {
	rr := &models.PSQkCouponRecommendRule{}
	rr.Ctype = int32(models.EQKCOUPON_CTYPE_RECHARGE_GIVE)
	rr.Sum = sum
	rr.Discount = discount
	rr.ExpiryDay = expiryDay
	return rr
}

func (p *CreateRecommendController) createDiscountCoupon(discount int32, expiryDay int32) *models.PSQkCouponRecommendRule {
	rr := &models.PSQkCouponRecommendRule{}
	rr.Ctype = int32(models.EQKCOUPON_CTYPE_WASH_DISCOUNT)
	rr.Discount = discount
	rr.ExpiryDay = expiryDay
	return rr
}

// @router /create [post]
func (p *CreateRecommendController) CreateRecommend() {
	defer p.Output("CreateRecommend")

	recommender, errCode := p.verification()
	if errCode != errorcode.EC_GL_SUCCESS {
		p.Resp["code"] = errCode
		return
	}

	rsp := &models.PCQkCouponCreateRecommendRsp{}
	rsp.RecommendId = recommender

	errCode = db.IsExistRecommendInfo(recommender)
	if errCode == errorcode.EC_GL_EXEC_SQL_FAIL ||
		errCode == errorcode.EC_QKCOUPON_ALREADY_EXIST_RECOMMEND {
		p.Resp["data"] = rsp
		p.Resp["code"] = errorcode.EC_GL_SUCCESS
		return
	}

	recommenderRule := p.createRule(models.EQKCOUPON_OPERATOR_RECOMMENDER)
	receiverRule := p.createRule(models.EQKCOUPON_OPERATOR_RECEIVER)

	errCode = db.CreateRecommendCoupon(recommender, recommenderRule, receiverRule)
	if errCode != errorcode.EC_GL_SUCCESS {
		p.Resp["code"] = errCode
		return
	}

	p.Resp["data"] = rsp
	p.Resp["code"] = errorcode.EC_GL_SUCCESS
}
