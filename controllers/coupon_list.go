/*
 * Copyrignt (c) doubimeizhi.com. All Rights Reserved.
 * Author: mr.gao<fbg@live.com>
 * Created Time: 2016-12-07 15:06
 * Last Modified: 2017-07-28 14:35:49
 * File Name: qkcoupon/controllers/coupon_list.go
 * Description:
 */

package controllers

import (
	"strings"
	"time"

	"qkcoupon/db"
	"qkcoupon/errorcode"
	"qkcoupon/models"
	"qkcoupon/rpc/client/passport"
	"qkcoupon/utils"

	log "github.com/cihub/seelog"
)

type CouponListController struct {
	LoginedController
}

/*
 *@note  验证请求参数
 *@return (uint, string, int32, int32, string, int32, int32, string, int32): (错误码, uid, cstate, ctype, bid, page_index, page_size, order, version)
 */
func (p *CouponListController) verification() (uint, string, int32, int32, string, int32, int32, string, int32) {
	var errCode uint = errorcode.EC_GL_SUCCESS
	var uid string = ""
	var cstate int32 = 0
	var ctype int32 = 0
	var bid string = ""
	var page_index int32 = 0
	var page_size int32 = 20
	var order string = ""
	var version int32 = 0

	for {
		var err error

		uid = p.GetString("uid")
		if uid == "" {
			log.Warn("uid==''")
			errCode = errorcode.EC_GL_INVALID_URL_PARAMS
			break
		}

		cstate, err = p.GetInt32("cstate", 0)
		if err == nil && cstate != 0 {
			if !utils.CheckCState(cstate) {
				log.Warn("EQKCOUPON_STATE_name[cstate] not exist")
				errCode = errorcode.EC_GL_INVALID_URL_PARAMS
				break
			}
		}

		ctype, err = p.GetInt32("ctype", 0)
		if err == nil && ctype != 0 {
			if !utils.CheckCType(ctype) {
				log.Warn("EQKCOUPON_TYPE_name[ctype] not exist")
				errCode = errorcode.EC_GL_INVALID_URL_PARAMS
				break
			}
		}

		bid = p.GetString("bid", "")

		page_index, _ = p.GetInt32("page_index", 0)
		page_size, _ = p.GetInt32("page_size", 20)

		order = p.GetString("order", "")
		if order != "" {
			order1 := strings.Split(order, ",")
			if len(order1) != 2 {
				log.Warn("len(order) != 2")
				errCode = errorcode.EC_GL_INVALID_URL_PARAMS
				break
			}

			if order1[0] != "ctype" && order1[0] != "cstate" && order1[0] != "create_tick" {
				log.Warnf("order[0]:%s", order1[0])
				errCode = errorcode.EC_GL_INVALID_URL_PARAMS
				break
			}

			if order1[1] != "desc" && order1[1] != "asc" {
				log.Warnf("order[1]:%s", order1[1])
				errCode = errorcode.EC_GL_INVALID_URL_PARAMS
				break
			}
		}

		version, _ = p.GetInt32("version", 0)

		break
	}

	return errCode, uid, cstate, ctype, bid, page_index, page_size, order, version
}

/*
 *@note 服务端数据转客户端数据
 *@param cucsl: *PSQkCouponUserInfoExList
 *@return *PCQkCouponUserInfoList
 */
func (p *CouponListController) s2c(cucsl *models.PSQkCouponUserInfoExList) *models.PCQkCouponUserInfoList {
	uil := &models.PCQkCouponUserInfoList{}
	uil.List = make([]*models.PCQkCouponUserInfo, 0)

	now := time.Now().Unix()

	for _, cucs := range cucsl.List {
		ui := &models.PCQkCouponUserInfo{}
		ui.Ctype = cucs.BaseInfo.Ctype
		ui.Lmode = cucs.BaseInfo.Lmode
		ui.Bid = cucs.BaseInfo.Bid
		ui.Rwids = cucs.BaseInfo.Rwids
		ui.Ccode = cucs.UserInfo.Ccode
		ui.Sum = cucs.BaseInfo.Sum
		ui.Discount = cucs.BaseInfo.Discount
		ui.MaxDiscount = cucs.BaseInfo.MaxDiscount
		ui.Source = utils.Utype2Str(cucs.BaseInfo.Utype)

		if cucs.BaseInfo.TimeRanges != nil {
			ui.TimeRange = utils.GetClientShowDateTime(cucs.BaseInfo.TimeRanges)
		} else {
			ui.TimeRange = utils.GetClientShowDateTime2(cucs.BaseInfo.StartTick, cucs.BaseInfo.EndTick)
		}

		token := p.GetString("ak", "")
		uid := p.GetString("uid", "")
		ui.UseRange = utils.GetUseRange(cucs.BaseInfo.Rwids, token, uid)

		if now > cucs.BaseInfo.EndTick {
			ui.Cstate = int32(models.EQKCOUPON_CSTATE_EXPIRED)
		} else {
			ui.Cstate = cucs.UserInfo.Cstate
		}

		uil.List = append(uil.List, ui)
	}

	return uil
}

// @router /list [get]
func (p *CouponListController) CouponList() {
	defer p.Output("CouponList")

	errCode, uid, cstate, ctype, bid, page_index, page_size, order, version := p.verification()
	if errCode != errorcode.EC_GL_SUCCESS {
		p.Resp["code"] = errCode
		return
	}

	up, errCode := passport.GetUserProfileByUID(uid)
	if errCode != errorcode.EC_GL_SUCCESS || up.Mobile == "" {
		p.Resp["code"] = errCode
		return
	}

	cucsl, errCode := db.GetCouponList(uid, up.Mobile, cstate, ctype, bid, page_index, page_size, strings.Split(order, ","), version)

	p.Resp["code"] = errCode

	if errCode == errorcode.EC_GL_SUCCESS {
		p.Resp["data"] = p.s2c(cucsl)
	}
}
