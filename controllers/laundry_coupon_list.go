/*
 * Copyrignt (c) doubimeizhi.com. All Rights Reserved.
 * Author: mr.gao<fbg@live.com>
 * Created Time: 2016-12-07 15:06
 * Last Modified: 2017-07-28 14:38:04
 * File Name: qkcoupon/controllers/laundry_coupon_list.go
 * Description:
 */

package controllers

import (
	"time"

	"qkcoupon/db"
	"qkcoupon/errorcode"
	"qkcoupon/models"
	"qkcoupon/rpc/client/passport"
	"qkcoupon/rpc/client/qingke"
	"qkcoupon/utils"

	log "github.com/cihub/seelog"
)

type LaundryCouponListController struct {
	LoginedController
}

/*
 *@note  验证请求参数
 *@return (uint, string, *QSQkCouponWashInfo, int32, int8, int32): (错误码, uid, *QSQkCouponWashInfo, sum, usable, version)
 */
func (p *LaundryCouponListController) verification() (uint, string, *models.QSQkCouponWashInfo, int32, int8, int32) {
	var errCode uint = errorcode.EC_GL_SUCCESS
	var uid string = ""
	var sum int32 = 0
	var usable int8 = 0
	var wi = &models.QSQkCouponWashInfo{}
	var version int32 = 0

	for {
		var err error

		uid = p.GetString("uid", "")
		if uid == "" {
			log.Warn("uid == ''")
			errCode = errorcode.EC_GL_INVALID_URL_PARAMS
			break
		}

		mode_id, err := p.GetInt64("mode_id", 0)
		if err != nil || mode_id == 0 {
			log.Warn("mode_id==0")
			errCode = errorcode.EC_GL_INVALID_URL_PARAMS
			break
		}

		wi, errCode = qingke.GetWashingmodeQueryByModeId(p.GetString("ak", ""), mode_id)
		if errCode != errorcode.EC_GL_SUCCESS {
			log.Warnf("uid:%s", uid)
			errCode = errorcode.EC_GL_INVALID_URL_PARAMS
			break
		}

		sum, err = p.GetInt32("sum", 0)
		if err != nil {
			log.Warn("sum not exist")
			errCode = errorcode.EC_GL_INVALID_URL_PARAMS
			break
		}

		usable, err = p.GetInt8("usable", 0)
		if err != nil {
			log.Warn("usable not exist")
			errCode = errorcode.EC_GL_INVALID_URL_PARAMS
			break
		}

		version, _ = p.GetInt32("version", 0)

		break
	}

	return errCode, uid, wi, sum, usable, version
}

/*
 *@note 服务端数据转客户端数据
 *@param usable: 不可用与可用
 *@param wi: *QSQkCouponWashInfo
 *@param cucsl: *PSQkCouponUserInfoExList
 *@param version: 版本号
 *@return *PCQkCouponLaundryInfoList
 */
func (p *LaundryCouponListController) s2c(usable int8, sum int32, wi *models.QSQkCouponWashInfo, cucsl *models.PSQkCouponUserInfoExList, version int32) *models.PCQkCouponLaundryInfoList {
	lil := &models.PCQkCouponLaundryInfoList{}
	lil.List = make([]*models.PCQkCouponLaundryInfo, 0)

	now := time.Now().Unix()

	var isTimeRange bool

	for _, cucs := range cucsl.List {
		//客户端版本小于为6,而且为代金劵，不返回
		if cucs.BaseInfo.Ctype == int32(models.EQKCOUPON_CTYPE_RECHARGE_CASHCOUPON) ||
			cucs.BaseInfo.Ctype == int32(models.EQKCOUPON_CTYPE_WASH_CASHCOUPON) {
			if version < 6 {
				continue
			}
		}

		//暂时不判断rid与wid
		isExistRwid := utils.IsExistRwid(wi.RoomId, wi.WasherId, cucs.BaseInfo.Rwids)

		//获取是否当前时间在可用的范围内的状态
		if cucs.BaseInfo.TimeRanges != nil {
			isTimeRange = utils.CheckGetTimeRange(cucs.BaseInfo.TimeRanges)
		} else {
			isTimeRange = utils.CheckStartTime2EndTime(cucs.BaseInfo.StartTick, cucs.BaseInfo.EndTick)
		}

		//必须是没有使用过的
		if cucs.UserInfo.Cstate != int32(models.EQKCOUPON_CSTATE_NOTUSED) {
			continue
		}

		if usable == 1 {
			//以下是可用页面显示逻辑

			if !isExistRwid {
				continue
			}

			//不在时间范围内
			if !isTimeRange {
				continue
			}

			//不是折扣优惠劵 和 不是免费优惠劵
			if cucs.BaseInfo.Ctype != int32(models.EQKCOUPON_CTYPE_WASH_DISCOUNT) &&
				cucs.BaseInfo.Ctype != int32(models.EQKCOUPON_CTYPE_WASH_CASHCOUPON) &&
				cucs.BaseInfo.Ctype != int32(models.EQKCOUPON_CTYPE_WASH_FREE) {
				continue
			}

			//不是所有洗衣模式 和 不是当前洗衣模式
			if cucs.BaseInfo.Lmode != int32(models.EQK_WASH_MODE_ALL) &&
				cucs.BaseInfo.Lmode != wi.WashModeId {
				continue
			}
		} else {
			//以下是不可用页面显示逻辑
			var ok bool

			//不是折扣优惠劵和不是免费优惠劵 而且 未过期的
			if (cucs.BaseInfo.Ctype != int32(models.EQKCOUPON_CTYPE_WASH_DISCOUNT) &&
				cucs.BaseInfo.Ctype != int32(models.EQKCOUPON_CTYPE_WASH_CASHCOUPON) &&
				cucs.BaseInfo.Ctype != int32(models.EQKCOUPON_CTYPE_WASH_FREE)) &&
				cucs.BaseInfo.EndTick > now {
				ok = true
			}

			//是折扣优惠劵和是免费优惠劵 而且 未过期的
			if cucs.BaseInfo.EndTick > now {
				if !isExistRwid {
					ok = true
				}

				//洗衣模式等于全部和不等于当前洗衣模式
				if cucs.BaseInfo.Lmode != wi.WashModeId &&
					cucs.BaseInfo.Lmode != int32(models.EQK_WASH_MODE_ALL) {
					ok = true
				}

				//洗衣模式等于当前洗衣模式或全部 而且 不在时间范围内
				if (cucs.BaseInfo.Lmode == wi.WashModeId ||
					cucs.BaseInfo.Lmode == int32(models.EQK_WASH_MODE_ALL)) && !isTimeRange {
					ok = true
				}
			}

			// 如果消费价格为0，一律不可使用
			if sum == 0 {
				ok = true
			}

			if !ok {
				continue
			}
		}

		li := &models.PCQkCouponLaundryInfo{}
		li.Ctype = cucs.BaseInfo.Ctype
		li.Lmode = cucs.BaseInfo.Lmode
		li.Ccode = cucs.UserInfo.Ccode
		li.Sum = cucs.BaseInfo.Sum
		li.Discount = cucs.BaseInfo.Discount
		li.MaxDiscount = cucs.BaseInfo.MaxDiscount
		li.Source = utils.Utype2Str(cucs.BaseInfo.Utype)

		if cucs.BaseInfo.TimeRanges != nil {
			li.TimeRange = utils.GetClientShowDateTime(cucs.BaseInfo.TimeRanges)
		} else {
			li.TimeRange = utils.GetClientShowDateTime2(cucs.BaseInfo.StartTick, cucs.BaseInfo.EndTick)
		}
		token := p.GetString("ak", "")
		uid := p.GetString("uid", "")
		li.UseRange = utils.GetUseRange(cucs.BaseInfo.Rwids, token, uid)
		lil.List = append(lil.List, li)
	}

	return lil
}

// @router /list [get]
func (p *LaundryCouponListController) LaundryCouponList() {
	defer p.Output("LaundryCouponList")

	errCode, uid, wi, sum, usable, version := p.verification()
	if errCode != errorcode.EC_GL_SUCCESS {
		p.Resp["code"] = errCode
		return
	}

	if sum == 0 && usable == 1 {
		lil := &models.PCQkCouponLaundryInfoList{}
		lil.List = make([]*models.PCQkCouponLaundryInfo, 0)

		p.Resp["code"] = errorcode.EC_GL_SUCCESS
		p.Resp["data"] = lil
		return
	}

	up, errCode := passport.GetUserProfileByUID(uid)
	if errCode != errorcode.EC_GL_SUCCESS || up.Mobile == "" {
		p.Resp["code"] = errCode
		return
	}

	cucsl, errCode := db.GetCouponListByUid(uid, up.Mobile)

	p.Resp["code"] = errCode

	if errCode == errorcode.EC_GL_SUCCESS {
		p.Resp["data"] = p.s2c(usable, sum, wi, cucsl, version)
	}
}
