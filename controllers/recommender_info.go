/*
 * Copyrignt (c) doubimeizhi.com. All Rights Reserved.
 * Author: mr.gao<fbg@live.com>
 * Created Time: 2016-12-07 15:06
 * Last Modified: 2017-07-28 14:37:47
 * File Name: qkcoupon/controllers/coupon_count.go
 * Description:
 */

package controllers

import (
	"qkcoupon/db"
	"qkcoupon/errorcode"
	"qkcoupon/rpc/client/passport"

	"qkcoupon/models"
)

type RecommenderInfoController struct {
	URLSignController
}

/*
 *@note  验证请求参数
 *@return (uint, string): (错误码, recommend_id)
 */
func (p *RecommenderInfoController) verification() (string, uint) {
	var errCode uint = errorcode.EC_GL_SUCCESS
	var recommend_id string = ""

	for {
		recommend_id = p.GetString("recommend_id", "")
		break
	}

	return recommend_id, errCode
}

// @router /info [get]
func (p *RecommenderInfoController) RecommenderInfo() {
	defer p.Output("RecommenderInfo")

	recommendId, errCode := p.verification()
	if errCode != errorcode.EC_GL_SUCCESS {
		p.Resp["code"] = errCode
		return
	}

	errCode = db.IsExistRecommendInfo(recommendId)
	if errCode != errorcode.EC_QKCOUPON_ALREADY_EXIST_RECOMMEND {
		p.Resp["code"] = errCode
		return
	}

	up, errCode := passport.GetUserProfileByUID(recommendId)
	if errCode != errorcode.EC_GL_SUCCESS {
		p.Resp["code"] = errorcode.EC_GL_REMOTE_REQ_FAIL
		return
	}

	rsp := &models.PCQkRecommenderInfoRsp{}
	rsp.HeadImg = up.Avatar

	if up.NickName == "" {
		rsp.Name = up.Mobile[0:3] + "******" + up.Mobile[len(up.Mobile)-2:]
	} else {
		runeNickName := []rune(up.NickName)
		length := len(runeNickName)
		if length <= 7 {
			rsp.Name = up.NickName
		} else {
			rsp.Name = string(runeNickName[:6]) + "..."
		}
	}

	p.Resp["code"] = errorcode.EC_GL_SUCCESS
	p.Resp["data"] = rsp
}
