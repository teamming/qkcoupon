/*
 * Copyrignt (c) doubimeizhi.com. All Rights Reserved.
 * Author: mr.gao<fbg@live.com>
 * Created Time: 2016-12-07 15:06
 * Last Modified: 2017-07-28 14:37:24
 * File Name: qkcoupon/controllers/coupon_count.go
 * Description:
 */

package controllers

import (
	"qkcoupon/db"
	"qkcoupon/errorcode"
	"qkcoupon/rpc/client/passport"

	"qkcoupon/models"
)

type SharerInfoController struct {
	URLSignController
}

/*
 *@note  验证请求参数
 *@return (uint, string): (错误码, share_id)
 */
func (p *SharerInfoController) verification() (uint, string) {
	var errCode uint = errorcode.EC_GL_SUCCESS
	var share_id string = ""

	for {
		share_id = p.GetString("share_id", "")
		break
	}

	return errCode, share_id
}

// @router /info [get]
func (p *SharerInfoController) SharerInfo() {
	defer p.Output("SharerInfo")

	errCode, share_id := p.verification()
	if errCode != errorcode.EC_GL_SUCCESS {
		p.Resp["code"] = errCode
		return
	}

	uid, errCode := db.GetSharerId(share_id)
	if errCode != errorcode.EC_GL_SUCCESS {
		p.Resp["code"] = errCode
		return
	}

	up, errCode := passport.GetUserProfileByUID(uid)
	if errCode != errorcode.EC_GL_SUCCESS {
		p.Resp["code"] = errorcode.EC_GL_REMOTE_REQ_FAIL
		return
	}

	rsp := &models.PCQkSharerInfoRsp{}

	if up.NickName == "" {
		rsp.Name = up.Mobile[0:3] + "******" + up.Mobile[len(up.Mobile)-2:]
	} else {
		runeNickName := []rune(up.NickName)
		length := len(runeNickName)
		if length <= 7 {
			rsp.Name = up.NickName
		} else {
			rsp.Name = string(runeNickName[:6]) + "..."
		}
	}

	errCode = db.IsGrabEnd(share_id)
	if errCode == errorcode.EC_QKCOUPON_COUPON_GRAB_END {
		rsp.GrabEnd = int32(models.EQKCOUPON_GRABEND_TYPE_ALREADY_GRABEND)
	} else {
		rsp.GrabEnd = int32(models.EQKCOUPON_GRABEND_TYPE_NO_GRABEND)
	}

	p.Resp["code"] = errorcode.EC_GL_SUCCESS
	p.Resp["data"] = rsp
}
