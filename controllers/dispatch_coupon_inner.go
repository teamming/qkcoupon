/*
 * Copyrignt (c) qkier.com. All Rights Reserved.
 * Author: zengxiaobao@qkier.com
 * Created Time: 2017-08-07 10:54:00
 * Last Modified: 2017-08-07 12:47:20
 * File Name: controllers/dispatch_coupon_inner.go
 * Description:
 */

//
package controllers

import (
	"encoding/json"

	"qkcoupon/db"
	"qkcoupon/errorcode"

	log "github.com/cihub/seelog"
)

//该接口仅供内部shell脚本使用
type DispatchCouponInner struct {
	BaseController
}

func (p *DispatchCouponInner) DispatchCouponInner() {
	defer p.Output("DispatchCouponInner")
	body := p.Ctx.Input.RequestBody
	log.Infof("recv body: %s", string(body))

	var req struct {
		Cid  int64    `json:"cid"`
		Uids []string `json:"uids"`
	}
	if err := json.Unmarshal(body, &req); err != nil {
		p.Resp["code"] = errorcode.EC_GL_INVALID_JSON_BODY
		p.Resp["msg"] = errorcode.Msg(errorcode.EC_GL_INVALID_JSON_BODY)
		return
	}

	if len(req.Uids) == 0 || req.Cid == 0 {
		p.Resp["code"] = errorcode.EC_GL_SUCCESS
		p.Resp["code"] = "ignore dispatch, cid or uids invalid"
		return
	}

	code := db.DispatchCoupon(req.Cid, req.Uids)
	p.Resp["code"] = code
	p.Resp["msg"] = errorcode.Msg(code)
}
