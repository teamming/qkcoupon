package main

import (
	"fmt"

	"doubimeizhi.com/utility"
	"github.com/astaxie/beego"
	log "github.com/cihub/seelog"

	_ "qkcoupon/cache"
	"qkcoupon/config"
	_ "qkcoupon/db"
	_ "qkcoupon/errorcode"
	_ "qkcoupon/routers"
	"qkcoupon/utils"
)

func init() {
	if !utility.CheckAndRecordPid("qkcoupon_server.pid") {
		fmt.Println("process already exists, so quit")
		return
	}

	logger, err := log.LoggerFromConfigAsFile("conf/seelog.xml")
	if err != nil {
		panic(err)
		return
	}

	log.ReplaceLogger(logger)
	defer logger.Flush()

	config.InitConf()
}

func main() {
	log.Info("Start server...")

	go utils.HandleSignals()

	beego.Run()
}
