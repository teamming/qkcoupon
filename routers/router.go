// @APIVersion 1.0.0
// @Title beego Test API
// @Description beego has a very cool tools to autogenerate documents for your API
// @Contact astaxie@gmail.com
// @TermsOfServiceUrl http://beego.me/
// @License Apache 2.0
// @LicenseUrl http://www.apache.org/licenses/LICENSE-2.0.html
package routers

import (
	"qkcoupon/controllers"

	"github.com/astaxie/beego"
)

func init() {
	beego.Router("/", &controllers.DefaultController{})
	beego.Router("/*", &controllers.DefaultController{})

	//V1内部API
	InApiV1 := beego.NewNamespace("/v1",
		beego.NSInclude(&controllers.CreateCouponController{}),
		beego.NSInclude(&controllers.RestoreCouponController{}),
		beego.NSInclude(&controllers.UseCouponController{}),
		/*beego.NSNamespace("/recharge",
			beego.NSInclude(&controllers.UseRechargeCouponController{}),
		),
		beego.NSNamespace("/laundry",
			beego.NSInclude(&controllers.UseLaundryCouponController{}),
		),*/
		beego.NSNamespace("/share",
			beego.NSInclude(&controllers.CreateShareCouponController{}),
		),
		beego.NSNamespace("/exchange",
			beego.NSInclude(&controllers.CreateExchangeController{}),
		),
	)
	beego.AddNamespace(InApiV1)

	//V1外部API
	OutApiV1 := beego.NewNamespace("/v1",
		beego.NSInclude(&controllers.CouponListController{}),
		beego.NSInclude(&controllers.CouponCountController{}),
		beego.NSInclude(&controllers.DelsCouponController{}),
		beego.NSNamespace("/recharge",
			beego.NSInclude(&controllers.RechargeCouponListController{}),
		),
		beego.NSNamespace("/laundry",
			beego.NSInclude(&controllers.LaundryCouponListController{}),
		),
		beego.NSNamespace("/share",
			beego.NSInclude(&controllers.ReceiveCouponController{}),
			beego.NSInclude(&controllers.ShareCouponListController{}),
		),
		beego.NSNamespace("/sharer",
			beego.NSInclude(&controllers.SharerInfoController{}),
		),
		beego.NSNamespace("/recommend",
			beego.NSInclude(&controllers.CreateRecommendController{}),
			beego.NSInclude(&controllers.ReceiveRecommendController{}),
			beego.NSInclude(&controllers.FirstWashingController{}),
		),
		beego.NSNamespace("/recommender",
			beego.NSInclude(&controllers.RecommenderInfoController{}),
		),
		beego.NSNamespace("/exchange",
			beego.NSInclude(&controllers.ReceiveExchangeController{}),
		),
	)
	beego.AddNamespace(OutApiV1)

	beego.Router("/event/1705/newuser", &controllers.Event201705Controller{}, "get,post:NewUser")
	beego.Router("/inner/dispatch", &controllers.DispatchCouponInner{}, "post:DispatchCouponInner")
}
