package routers

import (
	"github.com/astaxie/beego"
)

func init() {

	beego.GlobalControllerRouter["qkcoupon/controllers:CouponCountController"] = append(beego.GlobalControllerRouter["qkcoupon/controllers:CouponCountController"],
		beego.ControllerComments{
			Method: "CouponCount",
			Router: `/count`,
			AllowHTTPMethods: []string{"get"},
			Params: nil})

	beego.GlobalControllerRouter["qkcoupon/controllers:CouponListController"] = append(beego.GlobalControllerRouter["qkcoupon/controllers:CouponListController"],
		beego.ControllerComments{
			Method: "CouponList",
			Router: `/list`,
			AllowHTTPMethods: []string{"get"},
			Params: nil})

	beego.GlobalControllerRouter["qkcoupon/controllers:CreateCouponController"] = append(beego.GlobalControllerRouter["qkcoupon/controllers:CreateCouponController"],
		beego.ControllerComments{
			Method: "CreateCoupon",
			Router: `/create`,
			AllowHTTPMethods: []string{"post"},
			Params: nil})

	beego.GlobalControllerRouter["qkcoupon/controllers:CreateExchangeController"] = append(beego.GlobalControllerRouter["qkcoupon/controllers:CreateExchangeController"],
		beego.ControllerComments{
			Method: "CreateExchange",
			Router: `/create`,
			AllowHTTPMethods: []string{"post"},
			Params: nil})

	beego.GlobalControllerRouter["qkcoupon/controllers:CreateRecommendController"] = append(beego.GlobalControllerRouter["qkcoupon/controllers:CreateRecommendController"],
		beego.ControllerComments{
			Method: "CreateRecommend",
			Router: `/create`,
			AllowHTTPMethods: []string{"post"},
			Params: nil})

	beego.GlobalControllerRouter["qkcoupon/controllers:CreateShareCouponController"] = append(beego.GlobalControllerRouter["qkcoupon/controllers:CreateShareCouponController"],
		beego.ControllerComments{
			Method: "CreateShareCoupon",
			Router: `/create`,
			AllowHTTPMethods: []string{"post"},
			Params: nil})

	beego.GlobalControllerRouter["qkcoupon/controllers:DelsCouponController"] = append(beego.GlobalControllerRouter["qkcoupon/controllers:DelsCouponController"],
		beego.ControllerComments{
			Method: "DelsCoupon",
			Router: `/dels`,
			AllowHTTPMethods: []string{"post"},
			Params: nil})

	beego.GlobalControllerRouter["qkcoupon/controllers:FirstWashingController"] = append(beego.GlobalControllerRouter["qkcoupon/controllers:FirstWashingController"],
		beego.ControllerComments{
			Method: "FirstWashing",
			Router: `/first_washing`,
			AllowHTTPMethods: []string{"post"},
			Params: nil})

	beego.GlobalControllerRouter["qkcoupon/controllers:LaundryCouponListController"] = append(beego.GlobalControllerRouter["qkcoupon/controllers:LaundryCouponListController"],
		beego.ControllerComments{
			Method: "LaundryCouponList",
			Router: `/list`,
			AllowHTTPMethods: []string{"get"},
			Params: nil})

	beego.GlobalControllerRouter["qkcoupon/controllers:ReceiveCouponController"] = append(beego.GlobalControllerRouter["qkcoupon/controllers:ReceiveCouponController"],
		beego.ControllerComments{
			Method: "ReceiveCoupon",
			Router: `/receive`,
			AllowHTTPMethods: []string{"post"},
			Params: nil})

	beego.GlobalControllerRouter["qkcoupon/controllers:ReceiveExchangeController"] = append(beego.GlobalControllerRouter["qkcoupon/controllers:ReceiveExchangeController"],
		beego.ControllerComments{
			Method: "ReceiveExchange",
			Router: `/receive`,
			AllowHTTPMethods: []string{"post"},
			Params: nil})

	beego.GlobalControllerRouter["qkcoupon/controllers:ReceiveRecommendController"] = append(beego.GlobalControllerRouter["qkcoupon/controllers:ReceiveRecommendController"],
		beego.ControllerComments{
			Method: "ReceiveRecommend",
			Router: `/receive`,
			AllowHTTPMethods: []string{"post"},
			Params: nil})

	beego.GlobalControllerRouter["qkcoupon/controllers:RechargeCouponListController"] = append(beego.GlobalControllerRouter["qkcoupon/controllers:RechargeCouponListController"],
		beego.ControllerComments{
			Method: "RechargeCouponList",
			Router: `/list`,
			AllowHTTPMethods: []string{"get"},
			Params: nil})

	beego.GlobalControllerRouter["qkcoupon/controllers:RecommenderInfoController"] = append(beego.GlobalControllerRouter["qkcoupon/controllers:RecommenderInfoController"],
		beego.ControllerComments{
			Method: "RecommenderInfo",
			Router: `/info`,
			AllowHTTPMethods: []string{"get"},
			Params: nil})

	beego.GlobalControllerRouter["qkcoupon/controllers:RestoreCouponController"] = append(beego.GlobalControllerRouter["qkcoupon/controllers:RestoreCouponController"],
		beego.ControllerComments{
			Method: "RestoreCoupon",
			Router: `/restore`,
			AllowHTTPMethods: []string{"post"},
			Params: nil})

	beego.GlobalControllerRouter["qkcoupon/controllers:ShareCouponListController"] = append(beego.GlobalControllerRouter["qkcoupon/controllers:ShareCouponListController"],
		beego.ControllerComments{
			Method: "ShareCouponList",
			Router: `/list`,
			AllowHTTPMethods: []string{"get"},
			Params: nil})

	beego.GlobalControllerRouter["qkcoupon/controllers:SharerInfoController"] = append(beego.GlobalControllerRouter["qkcoupon/controllers:SharerInfoController"],
		beego.ControllerComments{
			Method: "SharerInfo",
			Router: `/info`,
			AllowHTTPMethods: []string{"get"},
			Params: nil})

	beego.GlobalControllerRouter["qkcoupon/controllers:UseCouponController"] = append(beego.GlobalControllerRouter["qkcoupon/controllers:UseCouponController"],
		beego.ControllerComments{
			Method: "UseCoupon",
			Router: `/use`,
			AllowHTTPMethods: []string{"post"},
			Params: nil})

	beego.GlobalControllerRouter["qkcoupon/controllers:UseLaundryCouponController"] = append(beego.GlobalControllerRouter["qkcoupon/controllers:UseLaundryCouponController"],
		beego.ControllerComments{
			Method: "UseLaundryCoupon",
			Router: `/use`,
			AllowHTTPMethods: []string{"post"},
			Params: nil})

	beego.GlobalControllerRouter["qkcoupon/controllers:UseRechargeCouponController"] = append(beego.GlobalControllerRouter["qkcoupon/controllers:UseRechargeCouponController"],
		beego.ControllerComments{
			Method: "UseRechargeCoupon",
			Router: `/use`,
			AllowHTTPMethods: []string{"post"},
			Params: nil})

}
