package db

import (
	"doubimeizhi.com/utility"

	"github.com/astaxie/beego"
	"github.com/garyburd/redigo/redis"
)

var (
	qingkeMysqlHelper       *utility.MySQLHelper
	washerNearbyRedisPool   *redis.Pool
	washroomNearbyRedisPool *redis.Pool
)

func createQingkeMySQLHelper() (*utility.MySQLHelper, error) {
	mysqlHost := beego.AppConfig.DefaultString("mysql::mysql_host", "127.0.0.1")
	mysqlPort := beego.AppConfig.DefaultInt("mysql::mysql_port", 4100)
	mysqlUser := beego.AppConfig.DefaultString("mysql::mysql_user", "root")
	mysqlPass := beego.AppConfig.DefaultString("mysql::mysql_password", "root")
	mysqlDB := beego.AppConfig.DefaultString("mysql::mysql_db", "qkcoupon")
	maxOpenConns := beego.AppConfig.DefaultInt("mysql::mysql_maxOpenConns", 10)
	maxIdleConns := beego.AppConfig.DefaultInt("mysql::mysql_maxIdleConns", 5)

	qingkeMysqlHelper = utility.NewMySQLHelper(mysqlHost, mysqlPort, mysqlUser, mysqlPass, mysqlDB, maxOpenConns, maxIdleConns)
	if err := qingkeMysqlHelper.Ping(); err != nil {
		return nil, err
	}

	return qingkeMysqlHelper, nil
}

func GetQingkeMySQLHelper() (*utility.MySQLHelper, error) {
	if qingkeMysqlHelper == nil {
		return createQingkeMySQLHelper()
	}
	return qingkeMysqlHelper, nil
}

func init() {
	if _, err := GetQingkeMySQLHelper(); err != nil {
		panic("Failed to connect to Mysql!")
	}
}
