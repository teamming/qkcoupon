package db

import (
	"fmt"
	"qkcoupon/errorcode"
	"qkcoupon/models"
	"reflect"

	"doubimeizhi.com/utility"
	log "github.com/cihub/seelog"
)

/*
 *@note  批量删除优惠劵
 *@param uid: 用户ID
 *@param ccodes: 优惠券状态
 *@return uint: 错误码
 */
func DelCouponList(uid string, mobile string, ccodes []string) uint {
	if len(ccodes) == 0 {
		return errorcode.EC_QKCOUPON_DEL_COUPON_FAIL
	}

	var ccodes1 string

	for _, ccode := range ccodes {
		ccodes1 += "'" + ccode + "',"
	}

	ccodes1 = ccodes1[:len(ccodes1)-1]

	sql := fmt.Sprintf("DELETE FROM user_coupon")

	if uid != "" && mobile != "" {
		sql += fmt.Sprintf(" WHERE ccode IN (%s) AND (uid='%s' or mobile='%s')", ccodes1, uid, mobile)
	} else if uid != "" {
		sql += fmt.Sprintf(" WHERE ccode IN (%s) AND uid='%s'", ccodes1, uid)
	} else {
		sql += fmt.Sprintf(" WHERE ccode IN (%s) AND mobile='%s'", ccodes1, mobile)
	}

	log.Debugf("sql:%s", sql)

	_, err := qingkeMysqlHelper.Exec(sql)
	if err != nil {
		return errorcode.EC_QKCOUPON_DEL_COUPON_FAIL
	}

	return errorcode.EC_GL_SUCCESS
}

/*
 *@note  获取设置项
 *@param k: 键
 *@return (v, uint): (值, 错误码)
 */
func GetSettingItem(k string) ([]byte, uint) {
	sql := "SELECT v FROM setting WHERE k=?"
	//log.Debugf("sql:%s", sql)

	rows, err := qingkeMysqlHelper.Query(sql, k)
	if err != nil {
		return nil, errorcode.EC_GL_EXEC_SQL_FAIL
	}

	if len(rows) == 0 {
		return nil, errorcode.EC_QKCOUPON_SETTING_ITEM_NOT_EXIST
	}

	var v []byte
	utility.SetTypeValue(rows[0]["v"], reflect.ValueOf(&v).Elem())

	return v, errorcode.EC_GL_SUCCESS
}

/*
 *@note  通过接收者获取推荐者
 *@param uid: 用户ID
 *@param mobile: 手机号
 *@return (string, uint): (推荐者, 错误码)
 */
func GetRecommender(mobile string) (string, uint) {
	sql := "SELECT recommend_id FROM receive_recommend WHERE receiver=?"
	rows, err := qingkeMysqlHelper.Query(sql, mobile)
	if err != nil {
		return "", errorcode.EC_GL_EXEC_SQL_FAIL
	}

	if len(rows) == 0 {
		return "", errorcode.EC_QKCOUPON_RECOMMENDER_NOT_EXIST
	}

	var recommendId string
	utility.SetTypeValue(rows[0]["recommend_id"], reflect.ValueOf(&recommendId).Elem())

	return recommendId, errorcode.EC_GL_SUCCESS
}

/*
 *@note  是否调用过第一次洗衣
 *@param mobile: 手机号
 *@return (string, uint): (推荐者, 错误码)
 */
/*func IsFirstWashing(mobile string) uint {
	sql := "SELECT ccode FROM receive_recommend WHERE receiver=? AND `return`=1"
	rows, err := qingkeMysqlHelper.Query(sql, mobile)
	if err != nil {
		return errorcode.EC_GL_EXEC_SQL_FAIL
	}

	if len(rows) > 0 {
		return errorcode.EC_QKCOUPON_ALREADY_CALL_FIRST_WASHING
	}

	return errorcode.EC_GL_SUCCESS
}*/

func GetReceiveRecommendInfo(mobile string) (*models.TReceiveRecommendInfo, uint) {
	sql := "SELECT ccode,recommend_id,cid,receiver,`return` FROM receive_recommend WHERE receiver=?"
	rows, err := qingkeMysqlHelper.Query(sql, mobile)
	if err != nil {
		log.Warn(err.Error())
		return nil, errorcode.EC_GL_EXEC_SQL_FAIL
	}

	if len(rows) == 0 {
		return nil, errorcode.EC_QKCOUPON_RECOMMEND_NOT_EXIST
	}

	rri := &models.TReceiveRecommendInfo{}

	utility.SetTypeValue(rows[0]["ccode"], reflect.ValueOf(&rri.Ccode).Elem())
	utility.SetTypeValue(rows[0]["recommend_id"], reflect.ValueOf(&rri.RecommendId).Elem())
	utility.SetTypeValue(rows[0]["cid"], reflect.ValueOf(&rri.Cid).Elem())
	utility.SetTypeValue(rows[0]["receiver"], reflect.ValueOf(&rri.Receiver).Elem())
	utility.SetTypeValue(rows[0]["return"], reflect.ValueOf(&rri.Return).Elem())

	return rri, errorcode.EC_GL_SUCCESS
}
