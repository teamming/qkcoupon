package db

import (
	"fmt"
	"qkcoupon/errorcode"
	"time"

	"doubimeizhi.com/utility"
	log "github.com/cihub/seelog"
)

/*
 *@note  通过uid分发优惠劵信息
 *@param cid: 优惠劵ID
 *@param uids: 用户ID列表
 *@return uint: 错误码
 */
func DispatchCoupon(cid int64, uids []string) uint {
	if len(uids) == 0 {
		return errorcode.EC_QKCOUPON_DISPATCH_COUPON_FAIL
	}

	now := time.Now().Unix()

	sql := "INSERT INTO user_coupon(ccode,cid,uid,create_tick) VALUES "

	for _, uid := range uids {
		sql += fmt.Sprintf("('%s',%d,'%s',%d),", utility.GenerateUUIDToken(), cid, uid, now)
	}

	sql = sql[:len(sql)-1]

	//log.Debugf("sql:%s", sql)

	_, err := qingkeMysqlHelper.Exec(sql)
	if err != nil {
		log.Warn(err.Error())
		return errorcode.EC_QKCOUPON_DISPATCH_COUPON_FAIL
	}

	return errorcode.EC_GL_SUCCESS
}

/*
 *@note  通过uid分发优惠劵信息
 *@param cid: 优惠劵ID
 *@param uid: 用户ID
 *@return string: 用户优惠卷ID
 *@return uint: 错误码
 */
func DispatchCouponForCcode(cid int64, uid string) (string, uint) {
	if uid == "" {
		return "", errorcode.EC_QKCOUPON_DISPATCH_COUPON_FAIL
	}

	now := time.Now().Unix()
	ccode := utility.GenerateUUIDToken()

	sql := "INSERT INTO user_coupon(ccode,cid,uid,create_tick) VALUES "

	sql += fmt.Sprintf("('%s',%d,'%s',%d)", ccode, cid, uid, now)

	//log.Debugf("sql:%s", sql)

	_, err := qingkeMysqlHelper.Exec(sql)
	if err != nil {
		log.Warn(err.Error())
		return "", errorcode.EC_QKCOUPON_DISPATCH_COUPON_FAIL
	}

	return ccode, errorcode.EC_GL_SUCCESS
}

/*
 *@note  通过手机号分发分享优惠劵信息
 *@param share_id: 分享ID
 *@param cid: 优惠劵ID
 *@param mobile: 手机号
 *@return uint: 错误码
 */
func DispatchShareCouponByMobile(share_id string, cid int64, mobile string) (string, uint) {
	now := time.Now().Unix()
	ccode := utility.GenerateUUIDToken()

	sql := "INSERT INTO user_coupon(ccode,cid,mobile,create_tick) VALUES (?,?,?,?)"

	//log.Debugf("sql:%s", sql)
	//log.Debugf("%v, %v, %v, %v", ccode, cid, mobile, now)

	_, err := qingkeMysqlHelper.Exec(sql, ccode, cid, mobile, now)
	if err != nil {
		log.Warn(err.Error())
		return "", errorcode.EC_QKCOUPON_DISPATCH_COUPON_FAIL
	}

	sql = "INSERT INTO receive_share(ccode,share_id,cid) VALUES (?,?,?)"

	//log.Debugf("sql:%s", sql)
	//log.Debugf("%v, %v, %v", ccode, share_id, cid)

	_, err = qingkeMysqlHelper.Exec(sql, ccode, share_id, cid)
	if err != nil {
		log.Warn(err.Error())
		return "", errorcode.EC_QKCOUPON_DISPATCH_COUPON_FAIL
	}

	return ccode, errorcode.EC_GL_SUCCESS
}

/*
 *@note  分发优惠劵信息到接收者
 *@param cid: 基础优惠劵ID
 *@param mobile: 手机号
 *@return uint: 错误码
 */
func DispatchCouponByMobile(cid int64, mobile string) (string, uint) {
	now := time.Now().Unix()
	ccode := utility.GenerateUUIDToken()

	sql := "INSERT INTO user_coupon(ccode,cid,mobile,create_tick) VALUES (?,?,?,?)"
	_, err := qingkeMysqlHelper.Exec(sql, ccode, cid, mobile, now)
	if err != nil {
		log.Warn(err.Error())
		return "", errorcode.EC_QKCOUPON_DISPATCH_COUPON_FAIL
	}

	return ccode, errorcode.EC_GL_SUCCESS
}

/*
 *@note  分发推荐优惠劵信息到接收者
 *@param recommendId: 推荐ID
 *@param cid: 基础优惠劵ID
 *@param mobile: 手机号
 *@return uint: 错误码
 */
func DispatchRecommendToReceiveByMobile(recommendId string, cid int64, mobile string) (string, uint) {
	now := time.Now().Unix()
	ccode := utility.GenerateUUIDToken()

	sql := "INSERT INTO user_coupon(ccode,cid,mobile,create_tick) VALUES (?,?,?,?)"
	_, err := qingkeMysqlHelper.Exec(sql, ccode, cid, mobile, now)
	if err != nil {
		log.Warn(err.Error())
		return "", errorcode.EC_QKCOUPON_DISPATCH_COUPON_FAIL
	}

	sql = "INSERT INTO receive_recommend(ccode,recommend_id,cid,receiver) VALUES (?,?,?,?)"
	_, err = qingkeMysqlHelper.Exec(sql, ccode, recommendId, cid, mobile)
	if err != nil {
		log.Warn(err.Error())
		return "", errorcode.EC_QKCOUPON_DISPATCH_COUPON_FAIL
	}

	return ccode, errorcode.EC_GL_SUCCESS
}

/*
 *@note  分发推荐优惠劵信息到推荐者
 *@param recommendId: 推荐ID
 *@param cid: 优惠劵ID
 *@param mobile: 手机号
 *@return uint: 错误码
 */
func DispatchRecommendToRecommenderByMobile(recommendId string, cid int64, mobile string) (string, uint) {
	now := time.Now().Unix()
	ccode := utility.GenerateUUIDToken()

	sql := "INSERT INTO user_coupon(ccode,cid,uid,create_tick) VALUES (?,?,?,?)"
	_, err := qingkeMysqlHelper.Exec(sql, ccode, cid, recommendId, now)
	if err != nil {
		log.Warn(err.Error())
		return "", errorcode.EC_QKCOUPON_DISPATCH_COUPON_FAIL
	}

	sql = "UPDATE receive_recommend SET `return`=1 WHERE receiver=?"
	_, err = qingkeMysqlHelper.Exec(sql, mobile)
	if err != nil {
		log.Warn(err.Error())
		return "", errorcode.EC_QKCOUPON_DISPATCH_COUPON_FAIL
	}

	return ccode, errorcode.EC_GL_SUCCESS
}
