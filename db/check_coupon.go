package db

import (
	"qkcoupon/errorcode"
	"reflect"

	"doubimeizhi.com/utility"
	log "github.com/cihub/seelog"
)

/*
 *@note  分享优惠劵是否存在
 *@param share_id: 分享ID
 *@return uint: 错误码
 */
func IsExistShareCoupon(share_id string) uint {
	sql := "SELECT share_id FROM share_info WHERE share_id=?"

	//log.Debugf("sql:%s", sql)

	rows, err := qingkeMysqlHelper.Query(sql, share_id)
	if err != nil {
		return errorcode.EC_GL_EXEC_SQL_FAIL
	}

	if len(rows) == 0 {
		return errorcode.EC_QKCOUPON_COUPON_NOT_EXIST
	}

	return errorcode.EC_QKCOUPON_ALREADY_EXIST_SHARE
}

/*
 *@note  推荐有奖是否存在
 *@param recommend_id: 推荐ID
 *@return uint: 错误码
 */
func IsExistRecommendInfo(recommend_id string) uint {
	sql := "SELECT recommend_id FROM recommend_info WHERE recommend_id=?"
	rows, err := qingkeMysqlHelper.Query(sql, recommend_id)
	if err != nil {
		return errorcode.EC_GL_EXEC_SQL_FAIL
	}

	if len(rows) == 0 {
		return errorcode.EC_QKCOUPON_RECOMMEND_NOT_EXIST
	}

	return errorcode.EC_QKCOUPON_ALREADY_EXIST_RECOMMEND
}

/*
 *@note  是否领取过分享优惠劵
 *@param shareId: 分享ID
 *@param mobile: 手机号
 *@return uint: 错误码
 */
func IsReceiveShareCoupon(shareId string, mobile string) (string, uint) {
	sql := "SELECT a.ccode FROM user_coupon a LEFT JOIN receive_share b ON a.ccode=b.ccode WHERE b.share_id=? AND a.mobile=?"

	//log.Debugf("sql:%s", sql)

	rows, err := qingkeMysqlHelper.Query(sql, shareId, mobile)
	if err != nil {
		return "", errorcode.EC_GL_EXEC_SQL_FAIL
	}

	if len(rows) == 0 {
		return "", errorcode.EC_GL_SUCCESS
	}

	var ccode string
	utility.SetTypeValue(rows[0]["ccode"], reflect.ValueOf(&ccode).Elem())

	return ccode, errorcode.EC_QKCOUPON_ALREADY_RECEIVE_COUPON
}

/*
 *@note  是否领取过推荐优惠劵
 *@param shareId: 推荐ID
 *@param mobile: 手机号
 *@return uint: 错误码
 */
func IsReceiveRecommendCoupon(mobile string) (string, uint) {
	sql := "SELECT ccode FROM receive_recommend WHERE receiver=?"
	rows, err := qingkeMysqlHelper.Query(sql, mobile)
	if err != nil {
		return "", errorcode.EC_GL_EXEC_SQL_FAIL
	}

	if len(rows) == 0 {
		return "", errorcode.EC_GL_SUCCESS
	}

	var ccode string
	utility.SetTypeValue(rows[0]["ccode"], reflect.ValueOf(&ccode).Elem())

	return ccode, errorcode.EC_QKCOUPON_ALREADY_RECEIVE_RECOMMEND
}

/*
 *@note  是否被抢光
 *@param share_id: 分享ID
 *@return uint: 错误码
 */
func IsGrabEnd(share_id string) uint {
	sql := "SELECT share_id FROM share_info WHERE share_id=? and add_num>=total_num"

	//log.Debugf("sql:%s", sql)

	rows, err := qingkeMysqlHelper.Query(sql, share_id)
	if err != nil {
		return errorcode.EC_GL_EXEC_SQL_FAIL
	}

	if len(rows) == 0 {
		return errorcode.EC_GL_SUCCESS
	}

	return errorcode.EC_QKCOUPON_COUPON_GRAB_END
}

/*
 *@note  是否推荐有奖已用完
 *@param recommend_id: 推荐ID
 *@return uint: 错误码
 */
func IsRecommendEnd(recommend_id string) uint {
	sql := "SELECT recommend_id FROM recommend_info WHERE recommend_id=? and total_num>0 and add_num>=total_num"

	//log.Debugf("sql:%s", sql)

	rows, err := qingkeMysqlHelper.Query(sql, recommend_id)
	if err != nil {
		return errorcode.EC_GL_EXEC_SQL_FAIL
	}

	if len(rows) == 0 {
		return errorcode.EC_GL_SUCCESS
	}

	return errorcode.EC_QKCOUPON_COUPON_GRAB_END
}

/*
 *@note  重新计算已领的分享优惠劵数量
 *@param cid: 基础优惠劵ID
 *@return uint: 错误码
 */
func RecountShareCouponNum(share_id string, cid int64) uint {
	sql := "UPDATE share_info SET add_num=add_num+1 WHERE share_id=?"
	_, err := qingkeMysqlHelper.Exec(sql, share_id)
	if err != nil {
		log.Warn(err.Error())
		return errorcode.EC_GL_EXEC_SQL_FAIL
	}

	sql = "UPDATE coupon_info SET add_num=add_num+1 WHERE cid=?"
	_, err = qingkeMysqlHelper.Exec(sql, cid)
	if err != nil {
		log.Warn(err.Error())
		return errorcode.EC_GL_EXEC_SQL_FAIL
	}

	return errorcode.EC_GL_SUCCESS
}
