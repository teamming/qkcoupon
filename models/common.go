/*
 * Copyrignt (c) doubimeizhi.com. All Rights Reserved.
 * Author: mr.gao<fbg@live.com>
 * Created Time: 2016-12-05 09:48
 * Last Modified: 2016-12-05 09:48
 * File Name: models/common.go
 * Description:
 */
package models

type TRuleItem struct {
	Ctype     int32 `json:"ctype,omitempty"`
	Sum       int32 `json:"sum,omitempty"`
	Discount  int32 `json:"discount,omitempty"`
	Count     int32 `json:"count,omitempty"`
	ExpiryDay int32 `json:"expiry_day,omitempty"`
}

type TShareRule struct {
	Otype int32        `json:"otype,omitempty"`
	Items []*TRuleItem `json:"rule,omitempty"`
}

type TShareRuleList struct {
	Rules []*TShareRule `json:"rules,omitempty"`
}

type TRecommendRule struct {
	Operator int32        `json:"operator,omitempty"`
	Items    []*TRuleItem `json:"rule,omitempty"`
}

type TRecommendRuleList struct {
	Rules []*TRecommendRule `json:"rules,omitempty"`
}

type TReceiveRecommendInfo struct {
	Ccode       string
	RecommendId string
	Cid         int64
	Receiver    string
	Return      bool
}

//应用协议类型
const (
	ESCHEME_HTTP  string = "http"
	ESCHEME_HTTPS string = "https"
)

const DAY_UNIT int32 = 86400 //以秒为单位累计的1天

var ShareRuleMap = make(map[int32][]*TRuleItem)
var RecommendRuleMap = make(map[int32][]*TRuleItem)
