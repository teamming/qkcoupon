package models

// 变量名前缀说明：
// 第1-2位：P：程序对外输出的使用数据结构（猜测是provide），Q：程序向其他系统请求使用的数据结构（猜测是query），PQ：同时在上述两种情况下使用的数据结构（目前仅有用户信息使用本前缀，此结构体数据由passport rpc取得，并由查询结构输出）
// 第2/3位：S：服务端使用的数据，C：客户端使用的数据

// 基础优惠劵信息
type PSQkCouponBaseInfo struct {
	Cid         int64                 `json:"cid,omitempty"`
	Ctype       int32                 `json:"ctype,omitempty"`
	Utype       int32                 `json:"utype,omitempty"`
	Lmode       int32                 `json:"lmode,omitempty"`
	Bid         string                `json:"bid,omitempty"`
	Rwids       *PCQkCouponRwids      `json:"rwids,omitempty"`
	Sum         int32                 `json:"sum,omitempty"`
	Discount    int32                 `json:"discount,omitempty"`
	MaxDiscount int32                 `json:"max_discount,omitempty"`
	TimeRanges  *PCQkCouponTimeRanges `json:"time_ranges,omitempty"`
	StartTick   int64                 `json:"start_tick,omitempty"`
	EndTick     int64                 `json:"end_tick,omitempty"`
	AddNum      int32                 `json:"add_num,omitempty"`
	TotalNum    int32                 `json:"total_num,omitempty"`
}

// 用户优惠劵信息
type PSQkCouponUserInfo struct {
	Ccode      string `json:"ccode,omitempty"`
	Cid        int64  `json:"cid,omitempty"`
	Uid        string `json:"uid,omitempty"`
	Mobile     string `json:"mobile,omitempty"`
	Cstate     int32  `json:"cstate,omitempty"`
	CreateTick int64  `json:"create_tick,omitempty"`
	UseTick    int64  `json:"use_tick,omitempty"`
}

// 用户优惠劵与基础优惠劵信息
type PSQkCouponUserInfoEx struct {
	UserInfo *PSQkCouponUserInfo `json:"user_info,omitempty"`
	BaseInfo *PSQkCouponBaseInfo `json:"base_info,omitempty"`
}

// 用户优惠劵与基础优惠劵信息列表
type PSQkCouponUserInfoExList struct {
	List []*PSQkCouponUserInfoEx `json:"list,omitempty"`
}

// 创建优惠劵信息
type PSQkCouponCreateCouponReq struct {
	Sum         int32                 `json:"sum,omitempty"`
	Discount    int32                 `json:"discount,omitempty"`
	MaxDiscount int32                 `json:"max_discount,omitempty"`
	Bid         string                `json:"bid,omitempty"`
	Rwids       *PCQkCouponRwids      `json:"rwids,omitempty"`
	Ctype       int32                 `json:"ctype,omitempty"`
	Lmode       int32                 `json:"lmode,omitempty"`
	Utype       int32                 `json:"utype,omitempty"`
	Mobiles     string                `json:"mobiles,omitempty"`
	TimeRanges  *PCQkCouponTimeRanges `json:"time_ranges,omitempty"`
}

// 创建分享优惠劵信息
type PSQkCouponCreateShareCouponParam struct {
	ShareId       string `json:"share_id,omitempty"`
	OperationType int32  `json:"operation_type,omitempty"`
}

// 使用充值优惠劵信息
type PSQkCouponRechargeUseParam struct {
	Ccode string `json:"ccode,omitempty"`
	Sum   int32  `json:"sum,omitempty"`
}

// 恢复优惠劵状态参数
type PSQkCouponRestoreParam struct {
	Ccode string `json:"ccode,omitempty"`
}

// 使用洗衣优惠劵信息
type PSQkCouponLaundryUseParam struct {
	Ccode  string `json:"ccode,omitempty"`
	ModeId int64  `json:"mode_id,omitempty"`
	Sum    int32  `json:"sum,omitempty"`
}

// 洗衣机信息
type QSQkCouponWashInfo struct {
	ModeId       int64  `json:"mode_id,omitempty"`
	WasherId     string `json:"washer_id,omitempty"`
	WashModeId   int32  `json:"wash_mode_id,omitempty"`
	WashModeName string `json:"wash_mode_name,omitempty"`
	RoomId       string `json:"room_id,omitempty"`
	RoomName     string `json:"room_name,omitempty"`
}

type QSQkCouponWashInfoRsp struct {
	Code uint32              `json:"code,omitempty"`
	Msg  string              `json:"msg,omitempty"`
	Data *QSQkCouponWashInfo `json:"data,omitempty"`
}

// 账号信息
type PQSQkCouponUserProfile struct {
	Code       int32  `json:"code,omitempty"`
	Msg        string `json:"msg,omitempty"`
	UserId     string `json:"userId,omitempty"`
	Mobile     string `json:"mobile,omitempty"`
	NickName   string `json:"nick_name,omitempty"`
	Gender     int32  `json:"gender,omitempty"`
	Avatar     string `json:"avatar,omitempty"`
	Slogan     string `json:"slogan,omitempty"`
	Birthday   string `json:"birthday,omitempty"`
	Idcard     string `json:"idcard,omitempty"`
	InviteCode string `json:"invite_code,omitempty"`
	RegionCode string `json:"region_code,omitempty"`
	RegionAddr string `json:"region_addr,omitempty"`
}

// 分享优惠劵规则列表
type PSQkCouponShareCouponRuleList struct {
	Count int32                        `json:"count,omitempty"`
	List  []*PSQkCouponShareCouponRule `json:"list,omitempty"`
}

// 分享优惠劵规则
type PSQkCouponShareCouponRule struct {
	Ctype     int32 `json:"ctype,omitempty"`
	Lmode     int32 `json:"lmode,omitempty"`
	Sum       int32 `json:"sum,omitempty"`
	Discount  int32 `json:"discount,omitempty"`
	Count     int32 `json:"count,omitempty"`
	ExpiryDay int32 `json:"expiry_day,omitempty"`
}

// 推荐有奖规则列表
type PSQkCouponRecommendRuleList struct {
	List []*PSQkCouponRecommendRule `json:"list,omitempty"`
}

// 推荐有奖规则
type PSQkCouponRecommendRule struct {
	Ctype     int32 `json:"ctype,omitempty"`
	Lmode     int32 `json:"lmode,omitempty"`
	Sum       int32 `json:"sum,omitempty"`
	Discount  int32 `json:"discount,omitempty"`
	ExpiryDay int32 `json:"expiry_day,omitempty"`
}

// 已领取的分享优惠劵列表
type PSQkCouponShareCouponList struct {
	List []*PSQkCouponShareCoupon `json:"list,omitempty"`
}

// 已领取的分享优惠劵
type PSQkCouponShareCoupon struct {
	Mobile   string `json:"mobile,omitempty"`
	Ctype    int32  `json:"ctype,omitempty"`
	Lmode    int32  `json:"lmode,omitempty"`
	Sum      int32  `json:"sum,omitempty"`
	Discount int32  `json:"discount,omitempty"`
}

// 使用洗衣优惠劵请求参数
type PSQkCouponUseReq struct {
	Ccode  string `json:"ccode,omitempty"`
	Sum    int32  `json:"sum,omitempty"`
	ModeId int64  `json:"mode_id,omitempty"`
	Flag   string `json:"flag"`
}

// 使用洗衣优惠劵返回参数
type PSQkCouponUseRsp struct {
	PayCoin    int32 `json:"pay_coin,omitempty"`
	CalcResult int32 `json:"calc_result,omitempty"`
}

// 用户优惠劵信息列表
type PCQkCouponUserInfoList struct {
	List []*PCQkCouponUserInfo `json:"list,omitempty"`
}

// 充值优惠券信息
type PCQkCouponRechargeInfo struct {
	Ctype       int32                `json:"ctype,omitempty"`
	Lmode       int32                `json:"lmode,omitempty"`
	Ccode       string               `json:"ccode,omitempty"`
	Sum         int32                `json:"sum,omitempty"`
	Discount    int32                `json:"discount,omitempty"`
	MaxDiscount int32                `json:"max_discount,omitempty"`
	TimeRange   *PCQkCouponTimeRange `json:"time_range,omitempty"`
	UseRange    string               `json:"use_range,omitempty"`
	Source      string               `json:"source,omitempty"`
}

// 充值优惠券信息列表
type PCQkCouponRechargeInfoList struct {
	List []*PCQkCouponRechargeInfo `json:"list,omitempty"`
}

// 洗衣优惠券信息
type PCQkCouponLaundryInfo struct {
	Ctype       int32                `json:"ctype,omitempty"`
	Lmode       int32                `json:"lmode,omitempty"`
	Ccode       string               `json:"ccode,omitempty"`
	Sum         int32                `json:"sum,omitempty"`
	Discount    int32                `json:"discount,omitempty"`
	MaxDiscount int32                `json:"max_discount,omitempty"`
	TimeRange   *PCQkCouponTimeRange `json:"time_range,omitempty"`
	UseRange    string               `json:"use_range,omitempty"`
	Source      string               `json:"source,omitempty"`
}

// 洗衣优惠券信息列表
type PCQkCouponLaundryInfoList struct {
	List []*PCQkCouponLaundryInfo `json:"list,omitempty"`
}

// 优惠劵数量
type PCQkCouponInfoCount struct {
	Count int32 `json:"count,omitempty"`
}

// 批量删除优惠劵信息
type PCQkCouponDelsParam struct {
	Ccodes []string `json:"ccodes,omitempty"`
}

// 使用优惠劵后返回的信息
type PCQkCouponUseRetInfo struct {
	Ctype       int32 `json:"ctype,omitempty"`
	Sum         int32 `json:"sum,omitempty"`
	Discount    int32 `json:"discount,omitempty"`
	MaxDiscount int32 `json:"max_discount,omitempty"`
}

// PCRwid字段
type PCQkCouponRwid struct {
	Rid  string   `json:"rid,omitempty"`
	Wids []string `json:"wids,omitempty"`
}

// PCRwid字段列表
type PCQkCouponRwids struct {
	List []*PCQkCouponRwid `json:"list,omitempty"`
}

// PimeRange字段里Date与Time的结构
type PCQkCouponDataRange struct {
	Start string `json:"start,omitempty"`
	End   string `json:"end,omitempty"`
}

// PTimeRange字段
type PCQkCouponTimeRanges struct {
	Date []*PCQkCouponDataRange `json:"date,omitempty"`
	Time []*PCQkCouponDataRange `json:"time,omitempty"`
}

// PTimeRange字段
type PCQkCouponTimeRange struct {
	Date *PCQkCouponDataRange `json:"date,omitempty"`
	Time *PCQkCouponDataRange `json:"time,omitempty"`
}

// 领取分享优惠劵参数
type PCQkCouponReceiveCouponReq struct {
	ShareId string `json:"share_id,omitempty"`
	Mobile  string `json:"mobile,omitempty"`
}

// 领取分享优惠劵返回
type PCQkCouponReceiveCoupon struct {
	Name     string `json:"name,omitempty"`
	Ctype    int32  `json:"ctype,omitempty"`
	Lmode    int32  `json:"lmode,omitempty"`
	Sum      int32  `json:"sum,omitempty"`
	Discount int32  `json:"discount,omitempty"`
}

// 领取分享优惠劵返回列表
type PCQkCouponReceiveCouponRsp struct {
	List []*PCQkCouponReceiveCoupon `json:"list,omitempty"`
}

// 已领取的分享优惠劵列表
type PCQkCouponShareCouponList struct {
	List []string `json:"list,omitempty"`
}

// 获取分享者信息返回参数
type PCQkSharerInfoRsp struct {
	Name    string `json:"name,omitempty"`
	GrabEnd int32  `json:"grab_end,omitempty"`
}

// 领取推荐有奖请求
type PCQkCouponReceiveRecommendReq struct {
	RecommendId string `json:"recommend_id,omitempty"`
	Mobile      string `json:"mobile,omitempty"`
}

// 领取推荐有奖返回
type PCQkCouponReceiveRecommend struct {
	Name     string `json:"name,omitempty"`
	Ctype    int32  `json:"ctype,omitempty"`
	Lmode    int32  `json:"lmode,omitempty"`
	Sum      int32  `json:"sum,omitempty"`
	Discount int32  `json:"discount,omitempty"`
}

// 领取推荐有奖返回列表
type PCQkCouponReceiveRecommendRsp struct {
	List []*PCQkCouponReceiveRecommend `json:"list,omitempty"`
}

// 获取推荐者信息返回参数
type PCQkRecommenderInfoRsp struct {
	HeadImg      string `json:"head_img,omitempty"`
	Name         string `json:"name,omitempty"`
	RecommendEnd int32  `json:"recommend_end,omitempty"`
}

// 获取推荐者信息返回参数
type PCQkCouponCreateRecommendRsp struct {
	RecommendId string `json:"recommend_id,omitempty"`
}

// 用户优惠劵信息
type PCQkCouponUserInfo struct {
	Ctype       int32                `json:"ctype,omitempty"`
	Lmode       int32                `json:"lmode,omitempty"`
	Bid         string               `json:"bid,omitempty"`
	Rwids       *PCQkCouponRwids     `json:"rwids,omitempty"`
	Ccode       string               `json:"ccode,omitempty"`
	Cstate      int32                `json:"cstate,omitempty"`
	Sum         int32                `json:"sum,omitempty"`
	Discount    int32                `json:"discount,omitempty"`
	MaxDiscount int32                `json:"max_discount,omitempty"`
	TimeRange   *PCQkCouponTimeRange `json:"time_range,omitempty"`
	UseRange    string               `json:"use_range,omitempty"`
	Source      string               `json:"source,omitempty"`
}

// PSQkCouponExchangeCreateReq 新建一个优惠码的请求
type PSQkCouponExchangeCreateReq struct {
	Text        string                      `json:"text,omitempty"`
	Bid         string                      `json:"bid,omitempty"`
	StartTick   int64                       `json:"start_tick,omitempty"`
	EndTick     int64                       `json:"end_tick,omitempty"`
	UserLimit   int32                       `json:"user_limit,omitempty"`
	CouponLimit int32                       `json:"coupon_limit,omitempty"`
	NewUserOnly bool                        `json:"new_user,omitempty"`
	CouponRules *PSQkCouponExchangeRuleList `json:"coupon_rules,omitempty"`
}

// PSQkCouponExchangeInfo 优惠码信息结构
type PSQkCouponExchangeInfo struct {
	Text        string `json:"text,omitempty"`
	Bid         string `json:"bid,omitempty"`
	StartTick   int64  `json:"start_tick,omitempty"`
	EndTick     int64  `json:"end_tick,omitempty"`
	UserLimit   int32  `json:"user_limit,omitempty"`
	CouponLimit int32  `json:"coupon_limit,omitempty"`
	NewUserOnly bool   `json:"new_user,omitempty"`
}

// PSQkCouponExchangeStatus 查询优惠码信息的结果
type PSQkCouponExchangeStatus struct {
	ExchangeID  string `json:"exchange_id,omitempty"`
	UserLimit   int32  `json:"user_limit,omitempty"`
	CouponCount int32  `json:"coupon_count,omitempty"`
	CouponLimit int32  `json:"coupon_limit,omitempty"`
	NewUserOnly bool   `json:"new_user,omitempty"`
}

// PSQkCouponExchangeRule 优惠码包含的优惠卷创建规则
type PSQkCouponExchangeRule struct {
	Bid         string           `json:"bid,omitempty"`
	Ctype       int32            `json:"ctype,omitempty"`
	Lmode       int32            `json:"lmode,omitempty"`
	Sum         int32            `json:"sum,omitempty"`
	Discount    int32            `json:"discount,omitempty"`
	MaxDiscount int32            `json:"max_discount,omitempty"`
	Rwids       *PCQkCouponRwids `json:"rwids,omitempty"`
}

// PSQkCouponExchangeRuleList 优惠码包含的优惠卷创建规则列表
type PSQkCouponExchangeRuleList struct {
	List []*PSQkCouponExchangeRule `json:"list,omitempty"`
}

// PCQkCouponExchangeRsp 兑换优惠码时返回的优惠卷列表
type PCQkCouponExchangeRsp struct {
	List []*PSQkCouponBaseInfo `json:"list,omitempty"`
}

// QSQkCouponWashRoomInfo 洗衣房名称
type QSQkCouponWashRoomInfo struct {
	RoomID   string `json:"room_id,omitempty"`
	RoomName string `json:"room_name,omitempty"`
}
