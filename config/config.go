/*
 * Copyrignt (c) doubimeizhi.com. All Rights Reserved.
 * Author: mr.gao<fbg@live.com>
 * Created Time: 2017-02-27 11:55
 * Last Modified: 2017-02-27 11:55
 * File Name: conf/config.go
 * Description:
 */
package config

import (
	"encoding/json"

	"qkcoupon/db"
	"qkcoupon/errorcode"
	"qkcoupon/models"
)

func InitConf() {
	initShareRule()
	initRecommendRule()
}

func initShareRule() {
	buf, errCode := db.GetSettingItem("share_rule")
	if errCode != errorcode.EC_GL_SUCCESS {
		panic(errorcode.Msg(errCode))
	}

	var srl = &models.TShareRuleList{}
	err := json.Unmarshal(buf, srl)
	if err != nil {
		panic(err.Error())
	}

	for _, v := range srl.Rules {
		models.ShareRuleMap[v.Otype] = v.Items
	}

	//check
	_, ok := models.ShareRuleMap[int32(models.EQKCOUPON_OPERATE_TYPE_WASH_PAYMENT_DONE)]
	if !ok {
		panic("rules error!")
	}

	_, ok = models.ShareRuleMap[int32(models.EQKCOUPON_OPERATE_TYPE_MONEY_RECHARGE_DONE)]
	if !ok {
		panic("rules error!")
	}
}

func initRecommendRule() {
	buf, errCode := db.GetSettingItem("recommend_rule")
	if errCode != errorcode.EC_GL_SUCCESS {
		panic(errorcode.Msg(errCode))
	}

	var rrl = &models.TRecommendRuleList{}
	err := json.Unmarshal(buf, rrl)
	if err != nil {
		panic(err.Error())
	}

	for _, v := range rrl.Rules {
		models.RecommendRuleMap[v.Operator] = v.Items
	}

	//check
	_, ok := models.RecommendRuleMap[int32(models.EQKCOUPON_OPERATOR_RECOMMENDER)]
	if !ok {
		panic("rules error!")
	}

	_, ok = models.RecommendRuleMap[int32(models.EQKCOUPON_OPERATOR_RECEIVER)]
	if !ok {
		panic("rules error!")
	}
}
