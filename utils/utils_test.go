/*
 * Copyrignt (c) doubimeizhi.com. All Rights Reserved.
 * Author: mr.gao<fbg@live.com>
 * Created Time: 2015-12-06 18:02:44
 * Last Modified: 2015-12-06 18:02:44
 * File Name: qkcoupon/utils/utils_test.go
 * Description:
 */
package utils

import (
	"testing"

	"qkcoupon/models"
)

func TestGetStartAndEndTick(t *testing.T) {
	trs := &models.PCQkCouponTimeRanges{}
	trs.Date = make([]*models.PCQkCouponDataRange, 0)
	trs.Time = make([]*models.PCQkCouponDataRange, 0)

	trs.Date = append(trs.Date, &models.PCQkCouponDataRange{Start: "2017.01.17", End: "2017.01.20"})

	trs.Time = append(trs.Time, &models.PCQkCouponDataRange{Start: "11:00", End: "23:59"})

	start_tick, end_tick, ok := GetStartAndEndTick(trs)

	t.Logf("start_tick:%d, end_tick:%d, ok:%v", start_tick, end_tick, ok)
}

func TestCheckGetTimeRange(t *testing.T) {
	trs := &models.PCQkCouponTimeRanges{}
	trs.Date = make([]*models.PCQkCouponDataRange, 0)
	trs.Time = make([]*models.PCQkCouponDataRange, 0)

	trs.Date = append(trs.Date, &models.PCQkCouponDataRange{Start: "2017.01.17", End: "2017.01.20"})

	trs.Time = append(trs.Time, &models.PCQkCouponDataRange{Start: "11:00", End: "23:59"})

	ok := CheckGetTimeRange(trs)

	t.Logf("ok:%v", ok)
}

func TestCheckStartTime2EndTime(t *testing.T) {
	ok := CheckStartTime2EndTime(1488211200, 1489593599)
	t.Logf("ok:%v", ok)
}

func TestCheckCreateTimeRange(t *testing.T) {
	trs := &models.PCQkCouponTimeRanges{}
	trs.Date = make([]*models.PCQkCouponDataRange, 0)
	trs.Time = make([]*models.PCQkCouponDataRange, 0)

	trs.Date = append(trs.Date, &models.PCQkCouponDataRange{Start: "0000.01.17", End: "2100.01.20"})

	trs.Time = append(trs.Time, &models.PCQkCouponDataRange{Start: "00:00", End: "23:59"})

	ok := CheckCreateTimeRange(trs)

	t.Logf("ok:%v", ok)
}

func TestGetClientShowDateTime(t *testing.T) {
	trs := &models.PCQkCouponTimeRanges{}
	trs.Date = make([]*models.PCQkCouponDataRange, 0)
	trs.Time = make([]*models.PCQkCouponDataRange, 0)

	trs.Date = append(trs.Date, &models.PCQkCouponDataRange{Start: "0000.01.17", End: "2016.01.20"})

	trs.Time = append(trs.Time, &models.PCQkCouponDataRange{Start: "00:00", End: "00:00"})

	tr := GetClientShowDateTime(trs)
	if tr == nil {
		t.Fatal("tr == nil")
	}

	t.Logf("PCQkCouponTimeRange:%v", tr)
}
