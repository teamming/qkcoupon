/*
 * Copyrignt (c) xuzeshui.com. All Rights Reserved.
 * Author: eason.gao
 * Created Time: 2015-08-22 14:38:49
 * Last Modified: 2016-11-22 21:03:03
 * File Name: auth/appkey.go
 * Description:
 */
package cache

import (
	"strconv"
	"sync"
	"time"

	log "github.com/cihub/seelog"
)

func init() {
	GetReceiveRecommendCount()
}

var (
	receiveRecommendCount *ReceiveRecommendCount
)

type ReceiveRecommendCount struct {
	m      map[string]int
	lock   *sync.RWMutex
	curDay int
}

func GetReceiveRecommendCount() *ReceiveRecommendCount {
	if receiveRecommendCount == nil {
		receiveRecommendCount = newReceiveRecommendCount()
		return receiveRecommendCount
	}
	return receiveRecommendCount
}

func newReceiveRecommendCount() *ReceiveRecommendCount {
	s := &ReceiveRecommendCount{}
	s.m = make(map[string]int)
	s.lock = new(sync.RWMutex)

	timestamp := time.Now().Unix()
	tm := time.Unix(timestamp, 0)
	s.curDay, _ = strconv.Atoi(tm.Format("02"))

	go s.clear()

	return s
}

func (p *ReceiveRecommendCount) Get(recommendId string, ip string) int {
	v := 0

	key := recommendId + "_" + ip
	log.Debugf("key:%s", key)

	p.lock.RLock()
	v, ok := p.m[key]
	p.lock.RUnlock()

	if !ok {
		v = 0
	}

	return v
}

func (p *ReceiveRecommendCount) Increase(recommendId string, ip string) int {
	var ret int

	key := recommendId + "_" + ip
	log.Debugf("key:%s", key)

	p.lock.RLock()
	v, ok := p.m[key]
	p.lock.RUnlock()

	if ok {
		ret = v + 1
	} else {
		ret = 1
	}

	p.lock.Lock()
	p.m[key] = ret
	p.lock.Unlock()

	return ret
}

func (p *ReceiveRecommendCount) clear() {
	for {
		timestamp := time.Now().Unix()
		tm := time.Unix(timestamp, 0)
		curDay, _ := strconv.Atoi(tm.Format("02"))

		if p.curDay != curDay {
			p.lock.Lock()
			p.m = make(map[string]int)
			p.lock.Unlock()
		}

		time.Sleep(10 * time.Second)
	}
}
