package errorcode

func msgshowInit() {
	//展示的msg
	errorMsgShow = make(map[uint]string)
	errorMsgShow[EC_GL_SUCCESS] = "成功"
	errorMsgShow[PM_LOGIN_PERMISSION_DENIDE] = "服务器开小差，稍后再试一下吧~~"
	errorMsgShow[EC_GL_INVALID_ACCESS_TOKEN] = "服务器开小差，稍后再试一下吧~"
	errorMsgShow[EC_GL_INVALID_APPKEY] = "服务器开小差，稍后再试一下吧~"
	errorMsgShow[EC_GL_INVALID_IP] = "服务器开小差，稍后再试一下吧~"
	errorMsgShow[EC_GL_INVALID_URL_TIME] = "服务器开小差，稍后再试一下吧~"
	errorMsgShow[EC_GL_INVALID_URL_SIGN] = "服务器开小差，稍后再试一下吧~"
	errorMsgShow[EC_GL_INVALID_SERVICE] = "服务器开小差，稍后再试一下吧~"
	errorMsgShow[EC_GL_INVALID_SESSIONID] = "服务器开小差，稍后再试一下吧~"
	errorMsgShow[EC_GL_INVALID_HTTPS] = "服务器开小差，稍后再试一下吧~"
	errorMsgShow[EC_GL_INVALID_URL_PARAMS] = "服务器开小差，稍后再试一下吧~ "
	errorMsgShow[EC_GL_EXEC_SQL_FAIL] = "服务器开小差，稍后再试一下吧~ "
	errorMsgShow[EC_GL_REMOTE_REQ_FAIL] = "服务器开小差，稍后再试一下吧~ "
	errorMsgShow[EC_GL_INVALID_JSON_BODY] = "服务器开小差，稍后再试一下吧~"
	errorMsgShow[EC_GL_INTERNAL_ERROR] = "服务器开小差，稍后再试一下吧~"
	errorMsgShow[EC_GL_MOBILE_FORMAT_ERROR] = "服务器开小差，稍后再试一下吧~"

	errorMsgShow[EC_QKCOUPON_COUPON_NOT_EMPTY] = "服务器开小差，稍后再试一下吧~"
	errorMsgShow[EC_QKCOUPON_COUPON_NOT_EXIST] = "服务器开小差，稍后再试一下吧~"
	errorMsgShow[EC_QKCOUPON_COUPON_INVALID] = "服务器开小差，稍后再试一下吧~"
	errorMsgShow[EC_QKCOUPON_COUPON_ALREADY_USED] = "服务器开小差，稍后再试一下吧~"
	errorMsgShow[EC_QKCOUPON_USE_COUPON_FAIL] = "服务器开小差，稍后再试一下吧~"
	errorMsgShow[EC_QKCOUPON_DEL_COUPON_FAIL] = "服务器开小差，稍后再试一下吧~"
	errorMsgShow[EC_QKCOUPON_CREATE_COUPON_FAIL] = "服务器开小差，稍后再试一下吧~"
	errorMsgShow[EC_QKCOUPON_GET_COUPON_FAIL] = "服务器开小差，稍后再试一下吧~"
	errorMsgShow[EC_QKCOUPON_DISPATCH_COUPON_FAIL] = "服务器开小差，稍后再试一下吧~"
	errorMsgShow[EC_QKCOUPON_GET_COUPON_COUNT_FAIL] = "服务器开小差，稍后再试一下吧~"
	errorMsgShow[EC_QKCOUPON_CREATE_SHARE_COUPON_FAIL] = "服务器开小差，稍后再试一下吧~"
	errorMsgShow[EC_QKCOUPON_ALREADY_RECEIVE_COUPON] = "服务器开小差，稍后再试一下吧~"
	errorMsgShow[EC_QKCOUPON_COUPON_GRAB_END] = "服务器开小差，稍后再试一下吧~"
	errorMsgShow[EC_QKCOUPON_ALREADY_EXIST_SHARE] = "服务器开小差，稍后再试一下吧~"
	errorMsgShow[EC_QKCOUPON_SETTING_ITEM_NOT_EXIST] = "服务器开小差，稍后再试一下吧~"
	errorMsgShow[EC_QKCOUPON_RECOMMENDER_NOT_EXIST] = "服务器开小差，稍后再试一下吧~"
	errorMsgShow[EC_QKCOUPON_CREATE_RECOMMEND_FAIL] = "服务器开小差，稍后再试一下吧~"
	errorMsgShow[EC_QKCOUPON_CANT_RECOMMEND_MYSELF] = "服务器开小差，稍后再试一下吧~"
	errorMsgShow[EC_QKCOUPON_RECOMMEND_NOT_EXIST] = "服务器开小差，稍后再试一下吧~"
	errorMsgShow[EC_QKCOUPON_ALREADY_EXIST_RECOMMEND] = "服务器开小差，稍后再试一下吧~"
	errorMsgShow[EC_QKCOUPON_ALREADY_RECEIVE_RECOMMEND] = "服务器开小差，稍后再试一下吧~"
	errorMsgShow[EC_QKCOUPON_ALREADY_CALL_FIRST_WASHING] = "服务器开小差，稍后再试一下吧~"
	errorMsgShow[EC_QKCOUPON_ALREADY_OLD_USER] = "服务器开小差，稍后再试一下吧~"
	errorMsgShow[EC_QKCOUPON_BIND_PEOPLE_OVER_RANGE] = "服务器开小差，稍后再试一下吧~"
	errorMsgShow[EC_QKCOUPON_DUPLICATE_EXHCANGE_TEXT] = "服务器开小差，稍后再试一下吧"
	errorMsgShow[EC_QKCOUPON_EXCHANGE_GRAB_END] = "服务器开小差，稍后再试一下吧"
	errorMsgShow[EC_QKCOUPON_INVALID_EXHCANGE_TEXT] = "服务器开小差，稍后再试一下吧"
}

func MsgShow(errcode uint) string {
	if msg, ok := errorMsgShow[errcode]; ok {
		return msg
	}
	return "未知错误"
}
