package errorcode

//业务错误码对应msg
func msgInit() {
	errorMsg = make(map[uint]string)
	errorMsg[EC_GL_SUCCESS] = "成功"
	errorMsg[PM_LOGIN_PERMISSION_DENIDE] = "您需要先登录才能操作哟~"
	errorMsg[EC_GL_INVALID_ACCESS_TOKEN] = "无效的access_token，未登录"
	errorMsg[EC_GL_INVALID_APPKEY] = "未授权的appkey"
	errorMsg[EC_GL_INVALID_IP] = "请求者的IP被封禁"
	errorMsg[EC_GL_INVALID_URL_TIME] = "URL时间戳差距过大，拒绝服务"
	errorMsg[EC_GL_INVALID_URL_SIGN] = "URL签名非法"
	errorMsg[EC_GL_INVALID_SERVICE] = "同步服务非法"
	errorMsg[EC_GL_INVALID_SESSIONID] = "会话id错误"
	errorMsg[EC_GL_INVALID_HTTPS] = "必须是https请求"
	errorMsg[EC_GL_INVALID_URL_PARAMS] = "URL参数非法 "
	errorMsg[EC_GL_EXEC_SQL_FAIL] = "执行SQL失败 "
	errorMsg[EC_GL_REMOTE_REQ_FAIL] = "远程请求失败 "
	errorMsg[EC_GL_INVALID_JSON_BODY] = "无法解析的body json字符串"
	errorMsg[EC_GL_INTERNAL_ERROR] = "服务器内部错误"
	errorMsg[EC_GL_MOBILE_FORMAT_ERROR] = "手机号格式错误"

	errorMsg[EC_QKCOUPON_COUPON_NOT_EMPTY] = "优惠券编码不能为空！"
	errorMsg[EC_QKCOUPON_COUPON_NOT_EXIST] = "该优惠券不存在！"
	errorMsg[EC_QKCOUPON_COUPON_INVALID] = "该优惠券已失效！"
	errorMsg[EC_QKCOUPON_COUPON_ALREADY_USED] = "该优惠券已使用！"
	errorMsg[EC_QKCOUPON_USE_COUPON_FAIL] = "使用优惠券失败，请重试！"
	errorMsg[EC_QKCOUPON_DEL_COUPON_FAIL] = "删除优惠券失败，请重试！"
	errorMsg[EC_QKCOUPON_CREATE_COUPON_FAIL] = "创建优惠券信息失败，请重试！"
	errorMsg[EC_QKCOUPON_GET_COUPON_FAIL] = "获取优惠券信息失败，请重试！"
	errorMsg[EC_QKCOUPON_DISPATCH_COUPON_FAIL] = "分发优惠券信息失败，请重试！"
	errorMsg[EC_QKCOUPON_GET_COUPON_COUNT_FAIL] = "获取优惠券总数量失败，请重试！"
	errorMsg[EC_QKCOUPON_CREATE_SHARE_COUPON_FAIL] = "创建分享优惠券信息失败，请重试！"
	errorMsg[EC_QKCOUPON_ALREADY_RECEIVE_COUPON] = "您已经领取过优惠劵啦，留点机会给别人吧！"
	errorMsg[EC_QKCOUPON_COUPON_GRAB_END] = "优惠劵已经被抢光啦！"
	errorMsg[EC_QKCOUPON_ALREADY_EXIST_SHARE] = "已经存在分享优惠劵！"
	errorMsg[EC_QKCOUPON_SETTING_ITEM_NOT_EXIST] = "设置项不存在！"
	errorMsg[EC_QKCOUPON_RECOMMENDER_NOT_EXIST] = "推荐者不存在！"
	errorMsg[EC_QKCOUPON_CREATE_RECOMMEND_FAIL] = "创建推荐有奖信息失败，请重试！"
	errorMsg[EC_QKCOUPON_CANT_RECOMMEND_MYSELF] = "自已不能领取新手福利！"
	errorMsg[EC_QKCOUPON_RECOMMEND_NOT_EXIST] = "推荐信息不存在！"
	errorMsg[EC_QKCOUPON_ALREADY_EXIST_RECOMMEND] = "推荐信息已经存在！"
	errorMsg[EC_QKCOUPON_ALREADY_RECEIVE_RECOMMEND] = "您已经领取过新手福利啦！"
	errorMsg[EC_QKCOUPON_ALREADY_CALL_FIRST_WASHING] = "第一次洗衣时已经调用过，不用重复调用！"
	errorMsg[EC_QKCOUPON_ALREADY_OLD_USER] = "已经是老用户，不能领取新手福利啦！"
	errorMsg[EC_QKCOUPON_BIND_PEOPLE_OVER_RANGE] = "绑定人数超过当天最大限度！"
	errorMsg[EC_QKCOUPON_DUPLICATE_EXHCANGE_TEXT] = "兑换码重复！"
	errorMsg[EC_QKCOUPON_EXCHANGE_GRAB_END] = "您已到达领取次数上限，留点机会给别人吧！"
	errorMsg[EC_QKCOUPON_INVALID_EXHCANGE_TEXT] = "兑换码不存在！"
}

func Msg(errcode uint) string {
	if msg, ok := errorMsg[errcode]; ok {
		return msg
	}
	return ""
}
